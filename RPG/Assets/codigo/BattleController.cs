﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleController : MonoBehaviour
{

    bool primerMenu = false;
    bool menuPelear = false;
    bool menuObj = false;
    bool objetivo = false;
    bool curar = false;
    bool bufar = false;
    bool habilidad = false;

    public GameObject[] selectores;
    public Creature[] personajes;
    List<Creature> turnos = new List<Creature>();
    int pointer;
    bool atacado;
    bool curado;
    bool bufeado;
    public GameObject[] opciones;
    public Text[] vidas;
    public Text[] magias;
    public Text[] textoMenu;
    public GameObject[] enemySelector;
    public GameObject[] aliadoSelector;
    public PlayerSelector[] playerSelector;
    int[] atq = new int[4];


    // Start is called before the first frame update
    void Start()
    {
        

        for (int i = 0; i < 4; i++)
        {
            personajes[i].hp = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].hp;
            personajes[i].maxhp = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].maxhp;
            personajes[i].mp = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].mp;
            personajes[i].maxmp = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].maxmp;
            personajes[i].spd = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].spd;
            personajes[i].atk = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].atk;
            personajes[i].mAtk = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].mAtk;
            personajes[i].exp = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].exp;
            personajes[i].lvl = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].lvl;
            personajes[i].habilidades = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].habilidades;
            personajes[i].spells = GameObject.Find("rojo_6").GetComponent<playerController>().party[i].spells;

        }

        atq[0] = personajes[0].atk;
        atq[1] = personajes[0].atk;
        atq[2] = personajes[0].atk;
        atq[3] = personajes[0].atk;
        

        for (int i = 0; i < personajes.Length; i++)
        {
            turnos.Add(personajes[i]);
        }
        turnos.Sort();

        Debug.Log(turnos[0]);
        Debug.Log(turnos[1]);
        Debug.Log(turnos[2]);
        Debug.Log(turnos[3]);
        Debug.Log(turnos[4]);

        pointer = 0;

        atacado = false;
        curado = false;
        bufeado = false;

        selectores[0].SetActive(true);
        selectores[1].SetActive(false);
        selectores[2].SetActive(false);
        selectores[3].SetActive(false);
        
        

        EnterFirstMenu();

    }

    //Esta es la funcion que activa el primer menu
    void EnterFirstMenu()
    {

        CambioPlayerSelector();

        

        primerMenu = true;
        menuPelear = false;
        objetivo = false;


        vidas[0].text = personajes[0].hp + "/" + personajes[0].maxhp;
        vidas[1].text = personajes[1].hp + "/" + personajes[1].maxhp;
        vidas[2].text = personajes[2].hp + "/" + personajes[2].maxhp;
        vidas[3].text = personajes[3].hp + "/" + personajes[3].maxhp;

        magias[0].text = personajes[0].mp + "/" + personajes[0].maxmp;
        magias[1].text = personajes[1].mp + "/" + personajes[1].maxmp;
        magias[2].text = personajes[2].mp + "/" + personajes[2].maxmp;
        magias[3].text = personajes[3].mp + "/" + personajes[3].maxmp;

        opciones[0].SetActive(true);
        opciones[1].SetActive(false);
        opciones[2].SetActive(false);

        textoMenu[0].gameObject.SetActive(true);
        textoMenu[1].gameObject.SetActive(true);
        textoMenu[2].gameObject.SetActive(true);
        textoMenu[3].gameObject.SetActive(true);
        textoMenu[4].gameObject.SetActive(false);
        textoMenu[5].gameObject.SetActive(false);
        textoMenu[6].gameObject.SetActive(false);
        textoMenu[7].gameObject.SetActive(false);
        textoMenu[8].gameObject.SetActive(false);
        textoMenu[9].gameObject.SetActive(false);

        selectores[4].SetActive(false);
        selectores[5].SetActive(false);
        selectores[6].SetActive(false);

        enemySelector[0].SetActive(false);
        enemySelector[1].SetActive(false);
        enemySelector[2].SetActive(false);
        enemySelector[3].SetActive(false);

        aliadoSelector[0].SetActive(false);
        aliadoSelector[1].SetActive(false);
        aliadoSelector[2].SetActive(false);
        aliadoSelector[3].SetActive(false);


        //Con esto te desplazas por el menu
        if (Input.GetKeyDown("d"))
        {

            if (selectores[0].activeSelf)
            {
                selectores[0].SetActive(false);
                selectores[1].SetActive(true);
            }
            else if (selectores[1].activeSelf)
            {
                selectores[1].SetActive(false);
                selectores[2].SetActive(true);
            }
            else if (selectores[2].activeSelf)
            {
                selectores[2].SetActive(false);
                selectores[3].SetActive(true);
            }
            else if (selectores[3].activeSelf)
            {
                selectores[3].SetActive(false);
                selectores[0].SetActive(true);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (selectores[0].activeSelf)
            {
                selectores[0].SetActive(false);
                selectores[3].SetActive(true);
            }
            else if (selectores[1].activeSelf)
            {
                selectores[1].SetActive(false);
                selectores[0].SetActive(true);
            }
            else if (selectores[2].activeSelf)
            {
                selectores[2].SetActive(false);
                selectores[1].SetActive(true);
            }
            else if (selectores[3].activeSelf)
            {
                selectores[3].SetActive(false);
                selectores[2].SetActive(true);
            }
        }
        //Con esto confirmas tu seleccion del menu
        if (Input.GetKeyDown("space"))
        {

            if (selectores[0].activeSelf)
            {
                //La primera opcion activa el menu de atacar
                selectores[4].SetActive(true);


                primerMenu = false;
                menuPelear = true;



            }
            else if (selectores[1].activeSelf)
            {
                //La segunda opcion activa el menu de objetos
                selectores[4].SetActive(true);
                primerMenu = false;
                menuObj = true;


            }
            else if (selectores[2].activeSelf)
            {

                //La tercera opcion activa el modo defensa del personaje
                turnos[pointer].defensa = true;
                pointer++;

            }
            else if (selectores[3].activeSelf)
            {
                //La cuarta opcion permite huir segun un random
                float escape = UnityEngine.Random.Range(0f, 1f);

                if (escape <= 0.5f)
                {
                    Escape();
                }
                else
                {
                    pointer++;
                }

            }

        }

    }

    //Esta funcion cambia el selector del jugador que actua en ese turno
    private void CambioPlayerSelector()
    {

        if (turnos[pointer].ranger == 1 && personajes[0].dead)
        {
            pointer++;
        }
        else if (turnos[pointer].ranger == 2 && personajes[1].dead)
        {
            pointer++;
        }
        else if (turnos[pointer].ranger == 3 && personajes[2].dead)
        {
            pointer++;
        }
        else if (turnos[pointer].ranger == 4 && personajes[3].dead)
        {
            pointer++;
        }


        if (turnos[pointer].ranger == 1)
        {
            playerSelector[0].gameObject.SetActive(true);
            playerSelector[1].gameObject.SetActive(false);
            playerSelector[2].gameObject.SetActive(false);
            playerSelector[3].gameObject.SetActive(false);
        }
        else if (turnos[pointer].ranger == 2)
        {
            playerSelector[0].gameObject.SetActive(false);
            playerSelector[1].gameObject.SetActive(true);
            playerSelector[2].gameObject.SetActive(false);
            playerSelector[3].gameObject.SetActive(false);
        }
        else if (turnos[pointer].ranger == 3)
        {
            playerSelector[0].gameObject.SetActive(false);
            playerSelector[1].gameObject.SetActive(false);
            playerSelector[2].gameObject.SetActive(true);
            playerSelector[3].gameObject.SetActive(false);
        }
        else if (turnos[pointer].ranger == 4)
        {
            playerSelector[0].gameObject.SetActive(false);
            playerSelector[1].gameObject.SetActive(false);
            playerSelector[2].gameObject.SetActive(false);
            playerSelector[3].gameObject.SetActive(true);
        }
    }

    //Esta funcion activa el menu de objetos
    private void EnterObjetosMenu()
    {

        selectores[0].SetActive(false);
        selectores[1].SetActive(false);
        selectores[2].SetActive(false);
        selectores[3].SetActive(false);


        opciones[0].SetActive(false);
        opciones[1].SetActive(true);
        opciones[2].SetActive(false);


        textoMenu[0].gameObject.SetActive(false);
        textoMenu[1].gameObject.SetActive(false);
        textoMenu[2].gameObject.SetActive(false);
        textoMenu[3].gameObject.SetActive(false);
        textoMenu[7].gameObject.SetActive(false);

        textoMenu[8].gameObject.SetActive(true);
        textoMenu[9].gameObject.SetActive(true);
        textoMenu[6].gameObject.SetActive(true);

        //Desplazamiento por el menu
        if (Input.GetKeyDown("d"))
        {

            if (selectores[4].activeSelf)
            {
                selectores[4].SetActive(false);
                selectores[5].SetActive(true);
            }
            else if (selectores[5].activeSelf)
            {
                selectores[5].SetActive(false);
                selectores[6].SetActive(true);
            }
            else if (selectores[6].activeSelf)
            {
                selectores[6].SetActive(false);
                selectores[4].SetActive(true);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (selectores[4].activeSelf)
            {
                selectores[4].SetActive(false);
                selectores[6].SetActive(true);
            }
            else if (selectores[5].activeSelf)
            {
                selectores[5].SetActive(false);
                selectores[4].SetActive(true);
            }
            else if (selectores[6].activeSelf)
            {
                selectores[6].SetActive(false);
                selectores[5].SetActive(true);
            }
        }

        //Confirmar seleccion del menu
        if (Input.GetKeyDown("space"))
        {

            if (selectores[4].activeSelf)
            {
                //La primera opcion activa el selector de aliado con el que usar el objeto pocion
                curar = true;
                menuObj = false;
                curado = false;
                aliadoSelector[0].SetActive(true);

            }
            else if (selectores[5].activeSelf)
            {
                //La segunda opcion activa el selector de aliado con el que usar el objeto ataque X
                menuObj = false;
                bufar = true;
                bufeado = false;

                aliadoSelector[0].SetActive(true);
            }
            else if (selectores[6].activeSelf)
            {
                //La tercera opcion vuelve al primer menu
                selectores[0].SetActive(true);
                selectores[1].SetActive(false);
                selectores[2].SetActive(false);
                selectores[3].SetActive(false);


                vidas[0].gameObject.SetActive(true);
                vidas[1].gameObject.SetActive(true);
                vidas[2].gameObject.SetActive(true);
                vidas[3].gameObject.SetActive(true);

                primerMenu = true;
                menuObj = false;


            }

        }


    }
    //Esta funcion activa el menu de pelear
    private void EnterPelearMenu()
    {

        selectores[0].SetActive(false);
        selectores[1].SetActive(false);
        selectores[2].SetActive(false);
        selectores[3].SetActive(false);


        opciones[0].SetActive(false);
        opciones[1].SetActive(true);
        opciones[2].SetActive(false);




        textoMenu[0].gameObject.SetActive(false);
        textoMenu[1].gameObject.SetActive(false);
        textoMenu[2].gameObject.SetActive(false);
        textoMenu[3].gameObject.SetActive(false);
        textoMenu[7].gameObject.SetActive(false);
        textoMenu[8].gameObject.SetActive(false);
        textoMenu[9].gameObject.SetActive(false);


        textoMenu[4].gameObject.SetActive(true);
        textoMenu[5].gameObject.SetActive(true);
        textoMenu[6].gameObject.SetActive(true);

        //Desplazamiento por el menu
        if (Input.GetKeyDown("d"))
        {

            if (selectores[4].activeSelf)
            {
                selectores[4].SetActive(false);
                selectores[5].SetActive(true);
            }
            else if (selectores[5].activeSelf)
            {
                selectores[5].SetActive(false);
                selectores[6].SetActive(true);
            }
            else if (selectores[6].activeSelf)
            {
                selectores[6].SetActive(false);
                selectores[4].SetActive(true);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (selectores[4].activeSelf)
            {
                selectores[4].SetActive(false);
                selectores[6].SetActive(true);
            }
            else if (selectores[5].activeSelf)
            {
                selectores[5].SetActive(false);
                selectores[4].SetActive(true);
            }
            else if (selectores[6].activeSelf)
            {
                selectores[6].SetActive(false);
                selectores[5].SetActive(true);
            }
        }

        //Confirmar seleccion
        if (Input.GetKeyDown("space"))
        {

            if (selectores[4].activeSelf)
            {

                //La primera opcion activa el selector de enemigo al que atacar con un ataque basico

                menuPelear = false;
                objetivo = true;

                atacado = false;

                enemySelector[0].SetActive(true);
                


            }
            else if (selectores[5].activeSelf)
            {

                //La segunda opcion activa el selector de enemigo al que atacar con una habilidad
                menuPelear = false;
                habilidad = true;

                atacado = false;

                enemySelector[0].SetActive(true);

            }
            else if (selectores[6].activeSelf)
            {
                //La tercera opcion vuelve al primer menu
                selectores[0].SetActive(true);
                selectores[1].SetActive(false);
                selectores[2].SetActive(false);
                selectores[3].SetActive(false);


                vidas[0].gameObject.SetActive(true);
                vidas[1].gameObject.SetActive(true);
                vidas[2].gameObject.SetActive(true);
                vidas[3].gameObject.SetActive(true);

                primerMenu = true;
                menuPelear = false;


            }

        }

    }
    //Esta es la funcion que controla a que aliado le subas el ataque con el objeto Ataque X
    private void EnterBufarMenu()
    {

        selectores[4].SetActive(false);
        selectores[5].SetActive(false);
        selectores[6].SetActive(false);

        opciones[0].SetActive(false);
        opciones[1].SetActive(false);
        opciones[2].SetActive(true);

        textoMenu[0].gameObject.SetActive(false);
        textoMenu[1].gameObject.SetActive(false);
        textoMenu[2].gameObject.SetActive(false);
        textoMenu[3].gameObject.SetActive(false);
        textoMenu[4].gameObject.SetActive(false);
        textoMenu[5].gameObject.SetActive(false);
        textoMenu[6].gameObject.SetActive(false);
        textoMenu[7].gameObject.SetActive(true);
        textoMenu[8].gameObject.SetActive(false);
        textoMenu[9].gameObject.SetActive(false);

        CambioAliadoSelector();


        if (bufeado)
        {

            if (Input.GetKeyDown("space"))
            {

                bufeado = false;


                pointer++;

                selectores[0].SetActive(true);
                selectores[1].SetActive(false);
                selectores[2].SetActive(false);
                selectores[3].SetActive(false);


                opciones[0].SetActive(true);
                opciones[1].SetActive(false);
                opciones[2].SetActive(false);

                vidas[0].gameObject.SetActive(true);
                vidas[1].gameObject.SetActive(true);
                vidas[2].gameObject.SetActive(true);
                vidas[3].gameObject.SetActive(true);

                primerMenu = true;
                bufar = false;

                textoMenu[0].gameObject.SetActive(true);
                textoMenu[1].gameObject.SetActive(true);
                textoMenu[2].gameObject.SetActive(true);
                textoMenu[3].gameObject.SetActive(true);
                textoMenu[4].gameObject.SetActive(false);
                textoMenu[5].gameObject.SetActive(false);
                textoMenu[6].gameObject.SetActive(false);
                textoMenu[7].gameObject.SetActive(false);
                textoMenu[8].gameObject.SetActive(false);
                textoMenu[9].gameObject.SetActive(false);
                textoMenu[7].text = "ESCOGE EL OBJETIVO";
                aliadoSelector[0].SetActive(false);
                aliadoSelector[1].SetActive(false);
                aliadoSelector[2].SetActive(false);
                aliadoSelector[3].SetActive(false);


            }


        }
        else
        {

            if (Input.GetKeyDown("space"))
            {


                if (aliadoSelector[0].activeSelf && !personajes[0].dead)
                {
                    personajes[0].atk = personajes[0].atk*2;
                    textoMenu[7].text = "Rojo ha sido bufado";

                    bufeado = true;

                }
                else if (aliadoSelector[1].activeSelf && !personajes[1].dead)
                {
                    personajes[1].atk = personajes[1].atk * 2;
                    textoMenu[7].text = "Azul ha sido bufado";

                    bufeado = true;
                }
                else if (aliadoSelector[2].activeSelf && !personajes[2].dead)
                {
                    personajes[2].atk = personajes[2].atk * 2;
                    textoMenu[7].text = "Verde ha sido bufado";

                    bufeado = true;
                }
                else if (aliadoSelector[3].activeSelf && !personajes[3].dead)
                {
                    personajes[3].atk = personajes[3].atk * 2;
                    textoMenu[7].text = "Amarillo ha sido bufado";

                    bufeado = true;
                }


            }
            else if (Input.GetKeyDown("f"))
            {

                menuObj = true;
                bufar = false;

                bufeado = false;

                selectores[4].SetActive(true);
                selectores[5].SetActive(false);
                selectores[6].SetActive(false);

                opciones[0].SetActive(false);
                opciones[1].SetActive(true);
                opciones[2].SetActive(false);

                textoMenu[7].text = "ESCOGE EL OBJETIVO";


                aliadoSelector[0].SetActive(false);
                aliadoSelector[1].SetActive(false);
                aliadoSelector[2].SetActive(false);
                aliadoSelector[3].SetActive(false);

                

            }
        }

    }
    //Esta es la funcion que controla a que aliado curas con el objeto pocion
    private void EnterCuracionMenu()
    {

        selectores[4].SetActive(false);
        selectores[5].SetActive(false);
        selectores[6].SetActive(false);

        opciones[0].SetActive(false);
        opciones[1].SetActive(false);
        opciones[2].SetActive(true);

        textoMenu[0].gameObject.SetActive(false);
        textoMenu[1].gameObject.SetActive(false);
        textoMenu[2].gameObject.SetActive(false);
        textoMenu[3].gameObject.SetActive(false);
        textoMenu[4].gameObject.SetActive(false);
        textoMenu[5].gameObject.SetActive(false);
        textoMenu[6].gameObject.SetActive(false);
        textoMenu[7].gameObject.SetActive(true);
        textoMenu[8].gameObject.SetActive(false);
        textoMenu[9].gameObject.SetActive(false);

        CambioAliadoSelector();


        if (curado)
        {

            if (Input.GetKeyDown("space"))
            {

                curado = false;


                pointer++;

                selectores[0].SetActive(true);
                selectores[1].SetActive(false);
                selectores[2].SetActive(false);
                selectores[3].SetActive(false);


                opciones[0].SetActive(true);
                opciones[1].SetActive(false);
                opciones[2].SetActive(false);

                vidas[0].gameObject.SetActive(true);
                vidas[1].gameObject.SetActive(true);
                vidas[2].gameObject.SetActive(true);
                vidas[3].gameObject.SetActive(true);

                primerMenu = true;
                curar = false;

                textoMenu[0].gameObject.SetActive(true);
                textoMenu[1].gameObject.SetActive(true);
                textoMenu[2].gameObject.SetActive(true);
                textoMenu[3].gameObject.SetActive(true);
                textoMenu[4].gameObject.SetActive(false);
                textoMenu[5].gameObject.SetActive(false);
                textoMenu[6].gameObject.SetActive(false);
                textoMenu[7].gameObject.SetActive(false);
                textoMenu[8].gameObject.SetActive(false);
                textoMenu[9].gameObject.SetActive(false);
                textoMenu[7].text = "ESCOGE EL OBJETIVO";
                aliadoSelector[0].SetActive(false);
                aliadoSelector[1].SetActive(false);
                aliadoSelector[2].SetActive(false);
                aliadoSelector[3].SetActive(false);


            }


        }
        else
        {

            if (Input.GetKeyDown("space"))
            {
                

                    if (aliadoSelector[0].activeSelf && !personajes[0].dead)
                    {
                        personajes[0].hp = personajes[0].maxhp;
                        textoMenu[7].text = "Rojo se ha curado todos los puntos de vida";
                        
                        curado = true;

                    }
                    else if (aliadoSelector[1].activeSelf && !personajes[1].dead)
                    {
                        personajes[1].hp = personajes[1].maxhp;
                        textoMenu[7].text = "Azul se ha curado todos los puntos de vida";
                        
                        curado = true;
                    }
                    else if (aliadoSelector[2].activeSelf && !personajes[2].dead)
                    {
                        personajes[2].hp = personajes[2].maxhp;
                        textoMenu[7].text = "Verde se ha curado todos los puntos de vida";
                        
                        curado = true;
                    }
                    else if (aliadoSelector[3].activeSelf && !personajes[3].dead)
                    {
                        personajes[3].hp = personajes[3].maxhp;
                        textoMenu[7].text = "Amarillo se ha curado todos los puntos de vida";
                        
                        curado = true;
                    }


            }
            else if (Input.GetKeyDown("f"))
            {

                menuObj = true;
                curar = false;

                curado = false;

                selectores[4].SetActive(true);
                selectores[5].SetActive(false);
                selectores[6].SetActive(false);

                opciones[0].SetActive(false);
                opciones[1].SetActive(true);
                opciones[2].SetActive(false);

                textoMenu[7].text = "ESCOGE EL OBJETIVO";


                aliadoSelector[0].SetActive(false);
                aliadoSelector[1].SetActive(false);
                aliadoSelector[2].SetActive(false);
                aliadoSelector[3].SetActive(false);

                

            }
        }


    }
    //Esta es la funcion que controla a que enemigo atacas con una habilidad
    private void EnterHabilidadMenu()
    {

        int porque = 0;

        if (pointer == 0)
        {
            porque = 1;
        }
        else if (pointer == 1)
        {
            porque = 0;
        }
        else if (pointer == 2)
        {
            porque = 3;
        }
        else if (pointer == 3)
        {
            porque = 2;
        }

        selectores[4].SetActive(false);
        selectores[5].SetActive(false);
        selectores[6].SetActive(false);

        opciones[0].SetActive(false);
        opciones[1].SetActive(false);
        opciones[2].SetActive(true);

        textoMenu[0].gameObject.SetActive(false);
        textoMenu[1].gameObject.SetActive(false);
        textoMenu[2].gameObject.SetActive(false);
        textoMenu[3].gameObject.SetActive(false);
        textoMenu[4].gameObject.SetActive(false);
        textoMenu[5].gameObject.SetActive(false);
        textoMenu[6].gameObject.SetActive(false);
        textoMenu[7].gameObject.SetActive(true);


        CambioEnemySelector();

        if (atacado)
        {

            if (Input.GetKeyDown("space"))
            {

                atacado = false;
                pointer++;

                selectores[0].SetActive(true);
                selectores[1].SetActive(false);
                selectores[2].SetActive(false);
                selectores[3].SetActive(false);


                opciones[0].SetActive(true);
                opciones[1].SetActive(false);
                opciones[2].SetActive(false);

                vidas[0].gameObject.SetActive(true);
                vidas[1].gameObject.SetActive(true);
                vidas[2].gameObject.SetActive(true);
                vidas[3].gameObject.SetActive(true);

                primerMenu = true;
                habilidad = false;

                textoMenu[0].gameObject.SetActive(true);
                textoMenu[1].gameObject.SetActive(true);
                textoMenu[2].gameObject.SetActive(true);
                textoMenu[3].gameObject.SetActive(true);
                textoMenu[4].gameObject.SetActive(false);
                textoMenu[5].gameObject.SetActive(false);
                textoMenu[6].gameObject.SetActive(false);
                textoMenu[7].gameObject.SetActive(false);
                textoMenu[7].text = "ESCOGE EL OBJETIVO";
                enemySelector[0].SetActive(false);
                enemySelector[1].SetActive(false);
                enemySelector[2].SetActive(false);
                enemySelector[3].SetActive(false);


            }


        }
        else
        {
            
            textoMenu[7].text = "Usar "+ GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.name + " contra";

            if (Input.GetKeyDown("space"))
            {

                //Con "space" confirmas el enemigo al que atacas y se muestra el daño

                if (enemySelector[0].activeSelf && !personajes[4].dead)
                {

                    personajes[4].hp -= turnos[pointer].atk* GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg;
                    if (personajes[porque].ranger == 1)
                    {
                        personajes[0].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 2)
                    {
                        personajes[1].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 3)
                    {
                        personajes[2].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 4)
                    {
                        personajes[3].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    textoMenu[7].text = "El ataque ha causado " + personajes[porque].atk * GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg + " de daño";

                    if (personajes[4].hp <= 0)
                    {
                        personajes[4].dead = true;
                        personajes[4].gameObject.SetActive(false);
                    }
                    atacado = true;

                }
                else if (enemySelector[1].activeSelf && !personajes[5].dead)
                {
                    personajes[5].hp -= personajes[porque].atk * GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg;
                    if (personajes[porque].ranger == 1)
                    {
                        personajes[0].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 2)
                    {
                        personajes[1].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 3)
                    {
                        personajes[2].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 4)
                    {
                        personajes[3].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    textoMenu[7].text = "El ataque ha causado " + personajes[porque].atk * GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg + " de daño";

                    if (personajes[5].hp <= 0)
                    {
                        personajes[5].dead = true;
                        personajes[5].gameObject.SetActive(false);
                    }
                    atacado = true;
                }
                else if (enemySelector[2].activeSelf && !personajes[6].dead)
                {
                    personajes[6].hp -= personajes[porque].atk * GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg;
                    if (personajes[porque].ranger == 1)
                    {
                        personajes[0].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 2)
                    {
                        personajes[1].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 3)
                    {
                        personajes[2].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 4)
                    {
                        personajes[3].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    textoMenu[7].text = "El ataque ha causado " + personajes[porque].atk * GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg + " de daño";

                    if (personajes[6].hp <= 0)
                    {
                        personajes[6].dead = true;
                        personajes[6].gameObject.SetActive(false);
                    }
                    atacado = true;
                }
                else if (enemySelector[3].activeSelf && !personajes[7].dead)
                {
                    personajes[7].hp -= personajes[porque].atk * GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg;
                    if (personajes[porque].ranger == 1)
                    {
                        personajes[0].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 2)
                    {
                        personajes[1].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 3)
                    {
                        personajes[2].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    else if (personajes[porque].ranger == 4)
                    {
                        personajes[3].mp -= GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.coste;
                    }
                    textoMenu[7].text = "El ataque ha causado " + personajes[porque].atk * GameObject.Find("rojo_6").GetComponent<playerController>().party[porque].habilidades.dmg + " de daño";

                    if (personajes[7].hp <= 0)
                    {
                        personajes[7].dead = true;
                        personajes[7].gameObject.SetActive(false);
                    }
                    atacado = true;
                }



            }
            else if (Input.GetKeyDown("f"))
            {

                //Con "f" cancelas el uso de la habilidad

                menuPelear = true;
                habilidad = false;

                atacado = false;

                selectores[4].SetActive(true);
                selectores[5].SetActive(false);
                selectores[6].SetActive(false);

                opciones[0].SetActive(false);
                opciones[1].SetActive(true);
                opciones[2].SetActive(false);

                textoMenu[7].text = "ESCOGE EL OBJETIVO";


                enemySelector[0].SetActive(false);
                enemySelector[1].SetActive(false);
                enemySelector[2].SetActive(false);
                enemySelector[3].SetActive(false);

                

            }
        }

    }
    //Esta es la funcion que controla a que enemigo atacas con un ataque basico
    private void EnterObjectiveMenu()
    {



            selectores[4].SetActive(false);
            selectores[5].SetActive(false);
            selectores[6].SetActive(false);

            opciones[0].SetActive(false);
            opciones[1].SetActive(false);
            opciones[2].SetActive(true);

            textoMenu[0].gameObject.SetActive(false);
            textoMenu[1].gameObject.SetActive(false);
            textoMenu[2].gameObject.SetActive(false);
            textoMenu[3].gameObject.SetActive(false);
            textoMenu[4].gameObject.SetActive(false);
            textoMenu[5].gameObject.SetActive(false);
            textoMenu[6].gameObject.SetActive(false);
            textoMenu[7].gameObject.SetActive(true);
        

        CambioEnemySelector();


        if (atacado)
        {

            if (Input.GetKeyDown("space"))
            {

                atacado = false;
                pointer++;

                selectores[0].SetActive(true);
                selectores[1].SetActive(false);
                selectores[2].SetActive(false);
                selectores[3].SetActive(false);


                opciones[0].SetActive(true);
                opciones[1].SetActive(false);
                opciones[2].SetActive(false);

                vidas[0].gameObject.SetActive(true);
                vidas[1].gameObject.SetActive(true);
                vidas[2].gameObject.SetActive(true);
                vidas[3].gameObject.SetActive(true);

                primerMenu = true;
                menuPelear = false;

                textoMenu[0].gameObject.SetActive(true);
                textoMenu[1].gameObject.SetActive(true);
                textoMenu[2].gameObject.SetActive(true);
                textoMenu[3].gameObject.SetActive(true);
                textoMenu[4].gameObject.SetActive(false);
                textoMenu[5].gameObject.SetActive(false);
                textoMenu[6].gameObject.SetActive(false);
                textoMenu[7].gameObject.SetActive(false);
                textoMenu[7].text = "ESCOGE EL OBJETIVO";
                enemySelector[0].SetActive(false);
                enemySelector[1].SetActive(false);
                enemySelector[2].SetActive(false);
                enemySelector[3].SetActive(false);


            }


        }
        else
        {

            if (Input.GetKeyDown("space"))
            {

                if (enemySelector[0].activeSelf && !personajes[4].dead)
                {
                    personajes[4].hp -= turnos[pointer].atk;
                    textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño";

                    if (personajes[4].hp <= 0)
                    {
                        personajes[4].dead = true;
                        personajes[4].gameObject.SetActive(false);
                    }
                    atacado = true;

                }
                else if (enemySelector[1].activeSelf && !personajes[5].dead)
                {
                    personajes[5].hp -= turnos[pointer].atk;
                    textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño";

                    if (personajes[5].hp <= 0)
                    {
                        personajes[5].dead = true;
                        personajes[5].gameObject.SetActive(false);
                    }
                    atacado = true;
                }
                else if (enemySelector[2].activeSelf && !personajes[6].dead)
                {
                    personajes[6].hp -= turnos[pointer].atk;
                    textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño";

                    if (personajes[6].hp <= 0)
                    {
                        personajes[6].dead = true;
                        personajes[6].gameObject.SetActive(false);
                    }
                    atacado = true;
                }
                else if (enemySelector[3].activeSelf && !personajes[7].dead)
                {
                    personajes[7].hp -= turnos[pointer].atk;
                    textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño";

                    if (personajes[7].hp <= 0)
                    {
                        personajes[7].dead = true;
                        personajes[7].gameObject.SetActive(false);
                    }
                    atacado = true;
                }

                

            }
            else if (Input.GetKeyDown("f"))
            {

                menuPelear = true;
                objetivo = false;

                atacado = false;

                selectores[4].SetActive(true);
                selectores[5].SetActive(false);
                selectores[6].SetActive(false);

                opciones[0].SetActive(false);
                opciones[1].SetActive(true);
                opciones[2].SetActive(false);

                textoMenu[7].text = "ESCOGE EL OBJETIVO";


                enemySelector[0].SetActive(false);
                enemySelector[1].SetActive(false);
                enemySelector[2].SetActive(false);
                enemySelector[3].SetActive(false);


                EnterPelearMenu();

            }
        }
        

    }
    //Esta es la funcion que cambia la flecha de seleccion de aliado
    private void CambioAliadoSelector()
    {

        if (Input.GetKeyDown("d"))
        {

            if (aliadoSelector[0].activeSelf)
            {
                aliadoSelector[0].SetActive(false);
                aliadoSelector[1].SetActive(true);
            }
            else if (aliadoSelector[1].activeSelf)
            {
                aliadoSelector[1].SetActive(false);
                aliadoSelector[2].SetActive(true);
            }
            else if (aliadoSelector[2].activeSelf)
            {
                aliadoSelector[2].SetActive(false);
                aliadoSelector[3].SetActive(true);
            }
            else if (aliadoSelector[3].activeSelf)
            {
                aliadoSelector[3].SetActive(false);
                aliadoSelector[0].SetActive(true);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (aliadoSelector[0].activeSelf)
            {
                aliadoSelector[0].SetActive(false);
                aliadoSelector[3].SetActive(true);
            }
            else if (aliadoSelector[1].activeSelf)
            {
                aliadoSelector[1].SetActive(false);
                aliadoSelector[0].SetActive(true);
            }
            else if (aliadoSelector[2].activeSelf)
            {
                aliadoSelector[2].SetActive(false);
                aliadoSelector[1].SetActive(true);
            }
            else if (aliadoSelector[3].activeSelf)
            {
                aliadoSelector[3].SetActive(false);
                aliadoSelector[2].SetActive(true);
            }
        }


    }


    //Esta es la funcion que cambia la flecha de seleccion de enemigo
    private void CambioEnemySelector()
    {
        if (Input.GetKeyDown("d"))
        {

            if (enemySelector[0].activeSelf)
            {
                enemySelector[0].SetActive(false);
                enemySelector[1].SetActive(true);
            }
            else if (enemySelector[1].activeSelf)
            {
                enemySelector[1].SetActive(false);
                enemySelector[2].SetActive(true);
            }
            else if (enemySelector[2].activeSelf)
            {
                enemySelector[2].SetActive(false);
                enemySelector[3].SetActive(true);
            }
            else if (enemySelector[3].activeSelf)
            {
                enemySelector[3].SetActive(false);
                enemySelector[0].SetActive(true);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (enemySelector[0].activeSelf)
            {
                enemySelector[0].SetActive(false);
                enemySelector[3].SetActive(true);
            }
            else if (enemySelector[1].activeSelf)
            {
                enemySelector[1].SetActive(false);
                enemySelector[0].SetActive(true);
            }
            else if (enemySelector[2].activeSelf)
            {
                enemySelector[2].SetActive(false);
                enemySelector[1].SetActive(true);
            }
            else if (enemySelector[3].activeSelf)
            {
                enemySelector[3].SetActive(false);
                enemySelector[2].SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        double tiempo = Time.time;

        if (turnos[pointer].player)
        {

            //Acciones en caso de ser un jugador

            if (primerMenu)
            {
                EnterFirstMenu();
            }
            else if (menuPelear)
            {

                EnterPelearMenu();

            }
            else if (objetivo)
            {

                EnterObjectiveMenu();

            }else if (menuObj)
            {

                EnterObjetosMenu();

            }else if (curar)
            {
                EnterCuracionMenu();
            }else if (bufar)
            {
                EnterBufarMenu();
            }else if (habilidad)
            {
                EnterHabilidadMenu();
            }

        }
        else
        {
            //Accion en caso de ser un enemigo
            AtaqueEnemigo();
        }

        //En caso de que todos los enemigos esten muertos se llama a la funcion victoria
        if(personajes[4].dead && personajes[5].dead && personajes[6].dead && personajes[7].dead)
        {
            Victoria();
        }

        Debug.Log(pointer);
        Debug.Log(tiempo);

    }
    //Esta es la funcion de escapar
    private void Escape()
    {

        GameObject.Find("rojo_6").GetComponent<playerController>().combatiendo = false;

        for (int i = 0; i < 4; i++)
        {
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].hp = personajes[i].hp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].maxhp = personajes[i].maxhp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].mp = personajes[i].mp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].maxmp = personajes[i].maxmp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].spd = personajes[i].spd;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].atk = atq[i];
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].mAtk = personajes[i].mAtk;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].exp = personajes[i].exp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].lvl = personajes[i].lvl;

        }

        SceneManager.LoadScene("Overworld");

    }
    //Esta es la funcion Victoria, que finaliza el combate
    private void Victoria()
    {

        personajes[0].exp += 100;
        personajes[1].exp += 100;
        personajes[2].exp += 100;
        personajes[3].exp += 100;

        if(personajes[0].exp == 100)
        {

            personajes[0].LevelUp();
            personajes[1].LevelUp();
            personajes[2].LevelUp();
            personajes[3].LevelUp();

        }

        GameObject.Find("rojo_6").GetComponent<playerController>().combatiendo = false;

        for (int i = 0; i < 4; i++)
        {
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].hp = personajes[i].hp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].maxhp = personajes[i].maxhp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].mp = personajes[i].mp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].maxmp = personajes[i].maxmp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].spd = personajes[i].spd;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].atk = atq[i];
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].mAtk = personajes[i].mAtk;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].exp = personajes[i].exp;
            GameObject.Find("rojo_6").GetComponent<playerController>().party[i].lvl = personajes[i].lvl;

        }
        
        SceneManager.LoadScene("Overworld");

    }
    //Esta es la funcion de atacar de los enemigos, que ataca de forma aleatoria a uno de los personajes
    private void AtaqueEnemigo()
    {
        if (!personajes[pointer].dead)
        {
            selectores[0].SetActive(false);
            selectores[2].SetActive(false);
            selectores[4].SetActive(false);
            selectores[5].SetActive(false);
            selectores[6].SetActive(false);

            
            opciones[0].SetActive(false);
            opciones[1].SetActive(false);
            opciones[2].SetActive(true);

            textoMenu[0].gameObject.SetActive(false);
            textoMenu[1].gameObject.SetActive(false);
            textoMenu[2].gameObject.SetActive(false);
            textoMenu[3].gameObject.SetActive(false);
            textoMenu[4].gameObject.SetActive(false);
            textoMenu[5].gameObject.SetActive(false);
            textoMenu[6].gameObject.SetActive(false);





            if (!atacado)
            {
                

                    if (UnityEngine.Random.Range(0, 4) == 0)
                    {

                        if (personajes[0].defensa)
                        {
                            personajes[0].hp -= (turnos[pointer].atk)/2;
                            textoMenu[7].text = "El ataque ha causado " + (turnos[pointer].atk) / 2 + " de daño a Rojo";
                        }
                        else
                        {
                            personajes[0].hp -= turnos[pointer].atk;
                            textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño a Rojo";
                        }

                        
                        textoMenu[7].gameObject.SetActive(true);
                        atacado = true;
                    }
                    else if (UnityEngine.Random.Range(0, 4) == 1)
                    {

                        if (personajes[1].defensa)
                        {
                            personajes[1].hp -= (turnos[pointer].atk) / 2;
                            textoMenu[7].text = "El ataque ha causado " + (turnos[pointer].atk) / 2 + " de daño a Azul";
                        }
                        else
                        {
                            personajes[1].hp -= turnos[pointer].atk;
                            textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño a Azul";
                        }
                        
                        textoMenu[7].gameObject.SetActive(true);
                        atacado = true;
                    }
                    else if (UnityEngine.Random.Range(0, 4) == 2)
                    {
                        if (personajes[2].defensa)
                        {
                            personajes[2].hp -= (turnos[pointer].atk) / 2;
                            textoMenu[7].text = "El ataque ha causado " + (turnos[pointer].atk) / 2 + " de daño a Verde";
                        }
                        else
                        {
                            personajes[2].hp -= turnos[pointer].atk;
                            textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño a Verde";
                        }
                        
                        textoMenu[7].gameObject.SetActive(true);
                        atacado = true;
                    }
                    else if (UnityEngine.Random.Range(0, 4) == 3)
                    {
                        if (personajes[3].defensa)
                        {
                            personajes[3].hp -= (turnos[pointer].atk) / 2;
                            textoMenu[7].text = "El ataque ha causado " + (turnos[pointer].atk) / 2 + " de daño a Amarillo";
                        }
                        else
                        {
                            personajes[3].hp -= turnos[pointer].atk;
                            textoMenu[7].text = "El ataque ha causado " + turnos[pointer].atk + " de daño a Amarillo";
                        }
                        textoMenu[7].gameObject.SetActive(true);
                        atacado = true;
                    }

                    
            }
            else
            {

                if (Input.GetKeyDown("space"))
                {


                    textoMenu[7].gameObject.SetActive(false);
                    textoMenu[7].text = "ESCOGE EL OBJETIVO";
                    atacado = false;

                    if (pointer == 7)
                    {
                        pointer = 0;
                    }
                    else
                    {
                        pointer++;
                    }

                    selectores[0].SetActive(true);

                }


            }

        }
        else
        {

            if (pointer == 7)
            {
                pointer = 0;
            }
            else
            {
                pointer++;
            }

        }
    }
}
