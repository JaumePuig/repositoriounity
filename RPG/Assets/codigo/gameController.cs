﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameController : MonoBehaviour
{

    public playerController player;

    public Camera gameCamera;

    static GameObject Controller = null;

    void Awake()
    {
        //Patró singleton
        if (Controller == null)
        {
            //crea el objecte en el primer moment
            Controller = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(Controller);
        }
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {

        if (SceneManager.GetActiveScene().name=="Overworld")
        {
            Invoke("Combate", Random.Range(4.0f, 10.0f));
            
        }

    }

    // Update is called once per frame
    void Update()
    {

        gameCamera.transform.position = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            gameCamera.transform.position.z);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "Moverse")
        {
            SceneManager.LoadScene("Overworld");
        }

        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "aPueblo")
        {
            SceneManager.LoadScene("Pruebas");
        }

    }

    void Combate()
    {

        SceneManager.LoadScene("Combat");
        

    }
}
