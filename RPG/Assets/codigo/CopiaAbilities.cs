﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CopiaAbilities
{

    public string name;
    public int coste;
    public int dmg;

}
