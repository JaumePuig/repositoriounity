﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CopiaCreature
{

    public int hp;
    public int maxhp;
    public int mp;
    public int maxmp;
    public int spd;
    public int atk;
    public int mAtk;
    public int lvl;
    public bool defensa;

    public bool player;
    public int ranger;
    public bool spells;
    public bool dead = false;
    public int exp = 0;
    public CopiaAbilities habilidades;

}
