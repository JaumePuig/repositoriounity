﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OverworldController : MonoBehaviour

    //Controlador del overworld en el cual mediante un random se entra en un combate
{

    public playerController player;

    public Camera gameCamera;

    // Start is called before the first frame update
    void Start()
    {

        Invoke("Combate", Random.Range(4.0f, 10.0f));

    }

    // Update is called once per frame
    void Update()
    {

        gameCamera.transform.position = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            gameCamera.transform.position.z);

    }

    void Combate()
    {

        SceneManager.LoadScene("Combat");



    }
    
}
