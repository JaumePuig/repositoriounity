﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour
{
    public GameObject cosa;
    public MenuInventario inventMenu;
    public int vel;
    //instancia del singleton
    static GameObject player = null;
    private Animator anim;
    public bool subo = false;
    public Creature[] party;
    public Camera gameCamera;
    bool pueblo;
    bool overworld;
    float comb;
    float aCombatir;
    bool inv;
    public bool combatiendo;
    int laugh;
    int ahora;
    AudioSource audioData;
    List<Item> inventario = new List<Item>();
    Creature[] alCargar = new Creature[4];
    public GameObject textBox;
    public Text txtDia;
    public GameObject rojo;
    public GameObject ping;
    public GameObject pinguBoss;
    public GameObject menace;
    bool dialogo1 = false;
    bool dialogo2 = false;
    bool dialogo3 = false;
    bool dialogo4 = false;
    bool dialogo5 = false;
    bool dialogo6 = false;
    bool dialogo7 = false;
    bool dialogo8 = false;
    bool dialogo9 = false;
    bool menace1 = false;
    int men;

    void Awake()
    {
        //Patró singleton
        if (player == null)
        {
            //crea el objecte en el primer moment
            player = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(player);
        }
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
        gameCamera = Camera.main;

    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        audioData = GetComponent<AudioSource>();

        textBox.SetActive(false);
        txtDia.gameObject.SetActive(false);
        rojo.SetActive(false);
        ping.SetActive(false);
        pinguBoss.SetActive(false);
        pinguBoss.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 1);
        menace.SetActive(false);
        men = 0;

        pueblo = true;

        comb = 0;
        aCombatir = Random.Range(200, 300);
        laugh = Random.Range(1000, 5000);

        inventario.Add(new Item("Pocion", "8 andaluz", "Cura un poco supongo"));
        inventario.Add(new Item("Ataque X", "Infinito", "Dobla el ataque"));
        inventario.Add(new Item("Poke ball", "1", "Desconoces su uso"));

        for(int i = 0; i<3; i++)
        {
            inventMenu.items[i].text = inventario[i].nom;
            inventMenu.cants[i].text = "x"+inventario[i].quant;
        }
        inventMenu.gameObject.SetActive(false);
        for (int i = 0; i < 3; i++)
        {
            inventMenu.items[i].gameObject.SetActive(false);
            inventMenu.cants[i].gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

        anim.SetFloat("Speed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));
        anim.SetFloat("Vert", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.y));
        if (combatiendo && !overworld)
        {

            
            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);

        }
        else if (inv)
        {
            //Este es el codigo que, al pulsar "f" cierra el inventario
            if (Input.GetKey("f"))
            {

                inv = false;
                inventMenu.gameObject.SetActive(false);

                for (int i = 0; i < 3; i++)
                {
                    inventMenu.items[i].gameObject.SetActive(false);
                    inventMenu.cants[i].gameObject.SetActive(false);

                }

            }

        }
        else
        {

            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            //Este es el codigo que, al pulsar "p" activa el inventario
            if (Input.GetKey("p"))
            {

                inv = true;
                inventMenu.gameObject.SetActive(true);
                transform.localScale = new Vector3(1f, 1f, 1f);
                inventMenu.gameObject.transform.localScale = new Vector3(52.09999f, 52.09999f, 52.09999f);

                for (int i = 0; i < 3; i++)
                {
                    inventMenu.items[i].gameObject.SetActive(true);
                    inventMenu.cants[i].gameObject.SetActive(true);
                    inventMenu.items[i].gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                    inventMenu.cants[i].gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                    
                }

            }

            //Este es el codigo para moverse a izquierda y derecha
            if (Input.GetKey("a"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                inventMenu.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                inventMenu.items[0].gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                transform.localScale = new Vector3(1f, 1f, 1f);


            }
            else if (Input.GetKey("d"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                inventMenu.gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);
                transform.localScale = new Vector3(-1f, 1f, 1f);
                inventMenu.items[0].gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);

            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);

            }
            //Este es el codigo para moverse arriba y abajo
            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                subo = true;
            }
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                subo = false;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);

            }

            //En caso de estar en el Overworld, empezara un contador para un encuentro aleatorio
            if (comb == aCombatir)
            {
                comb = 0;
                overworld = false;
                Invoke("Combate", 0.0f);


            }
            else if (overworld)
            {
                comb++;
            }

            //En caso de estar en el pueblo podras usar "g" y "h" para guardar o cargar la partida
            if (pueblo)
            {

                if (Input.GetKey("g"))
                {
                    this.saveGame();
                }
                if (Input.GetKey("h"))
                {
                    this.LoadGame();
                }

            }

        }

        //Esto es para ir a los dialogos con el Boss
        if (dialogo1)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            dialogos(1);



        }else if (dialogo2)
        {
            dialogos(2);
        }
        else if (dialogo3)
        {
            dialogos(3);
        }
        else if (dialogo4)
        {
            dialogos(4);
        }
        else if (dialogo5)
        {
            dialogos(5);
        }
        else if (dialogo6)
        {
            dialogos(6);
        }
        else if (dialogo7)
        {
            dialogos(7);
        }
        else if (dialogo8)
        {
            dialogos(8);
        }
        else if (dialogo9)
        {
            menace.SetActive(false);
            dialogo9 = false;
            SceneManager.LoadScene("combateCastillo");
        }

        //Esto hace que las letras amenazantes del boss se muevan
        if (menace.activeSelf)
        {
            if (menace1)
            {
                menace.transform.position = new Vector3(this.transform.position.x, 19.5f, 0f);
                
                men++;
                if (men == 5)
                {
                    menace1 = false;
                    men = 0;
                }
            }
            else
            {
                menace.transform.position = new Vector3(this.transform.position.x+0.25f, 19.5f, 0f);
                
                men++;
                if (men == 5)
                {
                    menace1 = true;
                    men = 0;
                }
            }
            

        }

        //Esto provoca que suene una risa de fondo de forma aleatoria
        if (ahora == laugh)
        {
            audioData.Play(0);
            laugh = Random.Range(1000, 5000);
            ahora = 0;
        }
        else
        {
            
            ahora++;
        }

        Debug.Log(aCombatir);
        Debug.Log(comb);
    }

    //Esta es la funcion de los dialogos, que cambia el texto y el retrato del personaje que habla en funcion del int que se le pase
    private void dialogos(int n)
    {


        vel = 0;

        textBox.SetActive(true);
        txtDia.gameObject.SetActive(true);

        if (n == 1)
        {

            rojo.SetActive(true);
            txtDia.text = "Un momento, no puede ser. Tu eres...";

            if (Input.GetKeyDown("space"))
            {

                dialogo1 = false;
                dialogo2 = true;

            }

        }else if(n == 2)
        {

            txtDia.text = "PINGU!!!";
            pinguBoss.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

            if (Input.GetKeyDown("space"))
            {

                dialogo2 = false;
                dialogo3 = true;

            }

        }
        else if (n == 3)
        {

            rojo.SetActive(false);
            ping.SetActive(true);

            txtDia.text = "Noot Nooot";
            menace.SetActive(true);

            if (Input.GetKeyDown("space"))
            {

                dialogo3 = false;
                dialogo4 = true;

            }

        }
        else if (n == 4)
        {

            rojo.SetActive(true);
            ping.SetActive(false);

            txtDia.text = "Porque has hecho esto?";

            if (Input.GetKeyDown("space"))
            {

                dialogo4 = false;
                dialogo5 = true;

            }
            

        }
        else if (n == 5)
        {

            rojo.SetActive(false);
            ping.SetActive(true);

            txtDia.text = "Nooot";

            if (Input.GetKeyDown("space"))
            {

                dialogo5 = false;
                dialogo6 = true;

            }

        }
        else if (n == 6)
        {

            rojo.SetActive(true);
            ping.SetActive(false);

            txtDia.text = "Como? Para liberarnos del yugo del capitalismo?";

            if (Input.GetKeyDown("space"))
            {

                dialogo6 = false;
                dialogo7 = true;

            }

        }
        else if (n == 7)
        {

            rojo.SetActive(true);
            ping.SetActive(false);

            txtDia.text = "Nos gusta el dinero. Y nos gusta SER HUMANOS!!";

            if (Input.GetKeyDown("space"))
            {

                dialogo7 = false;
                dialogo8 = true;

            }

        }
        else if (n == 8)
        {

            rojo.SetActive(false);
            ping.SetActive(false);

            txtDia.gameObject.SetActive(false);
            textBox.SetActive(false);

            if (Input.GetKeyDown("space"))
            {

                dialogo8 = false;
                dialogo9 = true;

            }

        }


    }

    //Esta funcion crea un archivo de guardado con la posicion actual del personaje y la lista de los miembros del grupo con sus estadisticas
    private Save createSave()
    {
        Save save = new Save();

        CopiaCreature[] falsaparty = new CopiaCreature[4];

        for(int i = 0; i < 4; i++)
        {

            CopiaCreature cpy = new CopiaCreature();

            cpy.atk = this.party[i].atk;
            cpy.hp = this.party[i].hp;
            cpy.maxhp = this.party[i].maxhp;
            cpy.mp = this.party[i].mp;
            cpy.maxmp = this.party[i].maxmp;
            cpy.spd = this.party[i].spd;
            cpy.mAtk = this.party[i].mAtk;
            cpy.lvl = this.party[i].lvl;
            cpy.defensa = this.party[i].defensa;
            cpy.player = this.party[i].player;
            cpy.ranger = this.party[i].ranger;
            cpy.spells = this.party[i].spells;
            cpy.dead = this.party[i].dead;
            cpy.exp = this.party[i].exp;

            CopiaAbilities cpyAbi = new CopiaAbilities();

            cpyAbi.name = this.party[i].habilidades.name;
            cpyAbi.coste = this.party[i].habilidades.coste;
            cpyAbi.dmg = this.party[i].habilidades.dmg;

            cpy.habilidades = cpyAbi;

            falsaparty[i] = cpy;

        }

        save.party = falsaparty;
        save.x = this.transform.position.x;
        save.y = this.transform.position.y;

        return save;

    }

    //Esta es la funcion que tras llamar al createSave, guarda los datos en un fichero
    private void saveGame()
    {
        //crees el objecte save
        Save save = this.createSave();
        //crees un BinaryFormatter que es com un OOS
        BinaryFormatter bf = new BinaryFormatter();
        //Crees el File
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        //serialitzes
        bf.Serialize(file, save);
        file.Close();

        Debug.Log("Game Saved");

    }

    //Esta funcion carga la partida guardada en caso de que haya, y sustituye la posicion del personaje y la lista de criaturas de la party con las del archivo guardado
    public void LoadGame()
    {
        // 1
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {


            // 2
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            // 3
            this.transform.position = new Vector2(save.x, save.y);

            for (int i = 0; i < 4; i++)
            {
                
                Abilities abi = (Abilities)Instantiate(this.party[i].habilidades, transform.position, transform.rotation);

                abi.name = save.party[i].habilidades.name;
                abi.coste = save.party[i].habilidades.coste;
                abi.dmg = save.party[i].habilidades.dmg;

                if(save.party[i].ranger == 1)
                {

                    GameObject.Find("Hero1").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero1").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero1").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero1").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero1").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero1").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero1").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero1").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero1").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero1").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero1").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero1").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero1").GetComponent<Creature>().habilidades = abi;

                }
                else if (save.party[i].ranger == 2)
                {
                    GameObject.Find("Hero2").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero2").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero2").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero2").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero2").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero2").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero2").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero2").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero2").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero2").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero2").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero2").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero2").GetComponent<Creature>().habilidades = abi;
                }
                else if (save.party[i].ranger == 3)
                {
                    GameObject.Find("Hero3").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero3").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero3").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero3").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero3").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero3").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero3").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero3").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero3").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero3").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero3").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero3").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero3").GetComponent<Creature>().habilidades = abi;
                }
                else if (save.party[i].ranger == 4)
                {
                    GameObject.Find("Hero4").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero4").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero4").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero4").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero4").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero4").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero4").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero4").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero4").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero4").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero4").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero4").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero4").GetComponent<Creature>().habilidades = abi;
                }

                

            }


            Debug.Log("Game Loaded");

        }
        else
        {
            Debug.Log("No game saved!");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Al colisionar con el objeto Moverse, se carga la escena del Overworld
        if (collision.gameObject.name == "Moverse")
        {
            
            this.transform.position = new Vector2(-4.5f, -3f);
            pueblo = false;
            overworld = true;
            //Invoke("Combate", Random.Range(4.0f, 10.0f));

            SceneManager.LoadScene("Overworld");
        }

        //Al colisionar con el objeto aPueblo, se carga la escena del pueblo
        if (collision.gameObject.name == "aPueblo")
        {

            this.transform.position = new Vector2(-3.5f, -11.3f);
            pueblo = true;
            overworld = false;
            comb = 0;

            SceneManager.LoadScene("Pruebas");
        }

        //Al colisionar con el objeto aCastillo, se carga la escena del castillo1
        if (collision.gameObject.name == "aCastillo")
        {

            this.transform.position = new Vector2(-3.5f, -9.5f);
            overworld = false;
            comb = 0;
            
            SceneManager.LoadScene("castillo");
        }

        //Al colisionar con el objeto aBoss, empieza la conversacion con el Boss
        if (collision.gameObject.name == "aBoss")
        {

            dialogo1 = true;
            pinguBoss.SetActive(true);


            
        }

        //Al colisionar con el objeto aOver, se carga la escena del Overworld
        if (collision.gameObject.name == "aOver")
        {
            
            this.transform.position = new Vector2(5.5f, 1.5f);
            overworld = true;
            pinguBoss.SetActive(false);
            Debug.Log("a Over");

            SceneManager.LoadScene("Overworld");
        }

        
    }

    //Funcion que carga la escena de combate contra mobs
    void Combate()
    {
        combatiendo = true;
        cosa.SetActive(false);
        SceneManager.LoadScene("Combat");
        cosa.SetActive(true);
    }

}
