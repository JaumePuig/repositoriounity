﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NPC : MonoBehaviour

    //Al colisionar con un NPC activara tanto su texto como el textbox y 3 segundos despues de desactivara
{
    public string texto;
    public Text txt;
    public GameObject txtbox;
    

    // Start is called before the first frame update
    void Start()
    {
        txt.gameObject.SetActive(false);
        txt.text = texto;
        txtbox.gameObject.SetActive(false);
    } 

    //Ejemplo copiado de internet para utilizar el WaitForSeconds
    IEnumerator Example()
    {
        yield return new WaitForSeconds(3);
        txt.gameObject.SetActive(false);
        txtbox.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {


    }

    //Colision contra el presonaje
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "rojo_6")
        {
            txt.text = texto;
            txt.gameObject.SetActive(true);
            txtbox.gameObject.SetActive(true);
            StartCoroutine(Example());

        }
    }

}
