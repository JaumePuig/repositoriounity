﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Creature : MonoBehaviour, System.IComparable<Creature>
    
    //Clase de la criatura, tanto nuestros personajes como enemigos
{

    public int hp;
    public int maxhp;
    public int mp;
    public int maxmp;
    public int spd;
    public int atk;
    public int mAtk;
    public int lvl;
    public bool defensa;

    public bool player;
    public int ranger;
    public bool spells;
    public bool dead = false;
    public int exp = 0;
    public Abilities habilidades;

    

    public int CompareTo(Creature other)
    {
        if (this.spd > other.spd)
        {
            return -1;
        }else if (this.spd < other.spd)
        {
            return 1;
        }
        else
        {
            return 1;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LevelUp()
    {

        if (ranger == 1)
        {
            maxhp += 15;
            spd += 3;
            atk += 4;
            mAtk += 3;
        }
        else if (ranger == 2)
        {
            maxhp += 10;
            spd += 4;
            atk += 5;
            mAtk += 2;
        }
        else if (ranger == 3)
        {
            maxhp += 20;
            spd += 1;
            atk += 2;
            mAtk += 1;
        }
        else if (ranger == 4)
        {
            maxhp += 5;
            spd += 2;
            atk += 6;
            mAtk = atk;
        }

        exp = 0;

    }

    //Hechizos/habilidades de nuestros personajes
    public void GiveSpells(int i)
    {

        if (i == 1)
        {

            habilidades = new Abilities("Corte", 3, 3);
            

        }
        else if (i == 2)
        {

            habilidades = new Abilities("Puñalada", 3,  4);
            

        }
        else if (i == 3)
        {
            habilidades = new Abilities("Toma ostia", 2, 2);
            
        }
        else if (i == 4)
        {
            habilidades = new Abilities("Fire I", 4, 4);
            
        }

    }

}
