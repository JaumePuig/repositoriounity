﻿using UnityEngine;
using System.Collections;

public class CheckpointController : MonoBehaviour
{

	#region Unity Fields
	public GameObject nextCheck;
    public GameObject prevCheck;
    public GameSceneManager manager;
    public GameObject previousArrow;
    public GameObject nextArrow;
    public bool activado;
    public bool firstTime;
    public bool first;
    #endregion Unity Fields

    #region Methods
    public void OnTriggerEnter (Collider collider)
	{

		if (collider.gameObject.name.Equals("Player"))
		{

            if (activado)
            {

                this.GetComponent<MeshRenderer>().enabled = false;
                //previousArrow.GetComponent<MeshRenderer>().enabled = false;
                previousArrow.SetActive(false);
                nextCheck.GetComponent<MeshRenderer>().enabled = true;
                //nextArrow.GetComponent<MeshRenderer>().enabled = true;
                nextArrow.SetActive(true);
                nextCheck.GetComponent<CheckpointController>().activado = true;
                activado = false;

                if (first)
                {

                    

                    if (!firstTime)
                    {

                        manager.started = true;
                        manager.finVuelta();
                        manager.totTime = 0f;

                    }
                    else
                    {

                        manager.totTime = 0f;

                    }

                }

                firstTime = false;

            }

        }

	}
	#endregion Methods
}