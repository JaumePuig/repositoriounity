﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameSceneManager : MonoBehaviour
{
	#region Unity Fields
	public Camera mainCamera;
	public Text scoreText;
	public Text gameOverText;
	public JugadorController player;
	public Transform checkpointContainer;
    [SerializeField]
    public BestTime bt;
	#endregion Unity Fields

	#region Fields
	private int score;
	private float gameTimer;
	private bool gameOver;
	public bool started;
	private float bestTime;
    public float totTime;
	#endregion Fields

	#region Methods
	public void Start ()
	{
		Time.timeScale = 1;

        RenderSettings.fog = true;
        RenderSettings.fogDensity = 0f;

        totTime = 0;
        started = true;

        bestTime = bt.bestTime;

		scoreText.text = "";
	}

	public void Update ()
	{
		// LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
		mainCamera.transform.position = new Vector3
		(
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
			Mathf.Lerp(mainCamera.transform.position.x, (player.transform.position.x - (player.transform.forward.x * 10)), Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.y, player.transform.position.y, Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.z, (player.transform.position.z - (player.transform.forward.z * 10)), Time.deltaTime * 10)
		);

        //fa una rotació automàtica per a que miri al jugador
		mainCamera.transform.LookAt(player.transform);

        totTime += Time.deltaTime;

		if (started)
		{
			scoreText.text = "Temps: " + totTime.ToString("F2");
			if (bestTime > -1)
			{
				scoreText.text += "\nMillor: " + (bestTime.ToString("F2"));
			}

        }
	}

    public void finVuelta()
    {

        if(totTime < bestTime || bestTime==0)
        {

            bt.bestTime = totTime;

        }

        bestTime = bt.bestTime;

    }
    
	#endregion Methods
}