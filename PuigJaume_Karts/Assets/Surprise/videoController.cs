﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class videoController : MonoBehaviour
{

    public GameObject vp;
    public AudioSource aSource;
    public bool corto;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider collider)
    {

        if (collider.gameObject.name.Equals("Player"))
        {

            vp.SetActive(true);
            if (corto)
            {

                StartCoroutine(Audio());

            }
            else
            {

                aSource.Stop();

            }
            

        }

    }

    public IEnumerator Audio()
    {

        aSource.Stop();

        yield return new WaitForSeconds(9f);

        aSource.Play();
        vp.SetActive(false);

    }

}
