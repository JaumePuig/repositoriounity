﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HechizoOscuridad : MonoBehaviour
{
    public float damage = 10f;
    float range = 100f;
    float momentum = 50f;
    public float manaConsumption = 9f;
    public PlayerData player;
    public Camera FPCamera;
    public GameObject bengala;
    public GameObject particules1;
    public GameObject particules2;
    public GameObject particules3;
    public GameObject particules4;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
        if (Input.GetKeyDown("1"))
        {
            this.gameObject.GetComponent<HechizoFuego>().enabled = true;
            this.gameObject.GetComponent<HechizoOscuridad>().enabled = false;
            particules1.SetActive(true);
            particules2.SetActive(false);
            particules3.SetActive(false);
            particules4.SetActive(false);
        }
        if (Input.GetKeyDown("2"))
        {
            this.gameObject.GetComponent<HechizoViento>().enabled = true;
            this.gameObject.GetComponent<HechizoOscuridad>().enabled = false;
            particules1.SetActive(false);
            particules2.SetActive(true);
            particules3.SetActive(false);
            particules4.SetActive(false);
        }
        if (Input.GetKeyDown("3"))
        {
            this.gameObject.GetComponent<HechizoOscuridad>().enabled = false;
            this.gameObject.GetComponent<HechizoAgua>().enabled = true;
            particules2.SetActive(false);
            particules4.SetActive(false);
            particules3.SetActive(true);
        }

    }

    private void Shoot()
    {

        if (player.mana >= manaConsumption)
        {

            player.mana -= manaConsumption;

            RaycastHit hit;


            if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
            {
                Debug.DrawLine(FPCamera.transform.position, hit.point, Color.red, 1f);
                print(hit.transform.name);
                

                if (hit.transform.tag == "Disparable")
                {
                    hit.transform.gameObject.GetComponent<Disparable>().dano(damage);
                    //Vector3 middle = new Vector3(hit.normal.x, )
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
                    hit.transform.gameObject.GetComponent<Pincha>().scary();
                }

                if (hit.transform.tag == "Player")
                {
                    hit.transform.gameObject.GetComponent<PlayerController>().dano(damage);
                    //Vector3 middle = new Vector3(hit.normal.x, )
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
                }
                GameObject newBengala = Instantiate(bengala);
                newBengala.transform.position = hit.point;
                Destroy(newBengala, 2f);


            }

        }

    }
}
