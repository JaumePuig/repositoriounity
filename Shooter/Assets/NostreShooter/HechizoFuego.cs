﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HechizoFuego : MonoBehaviour
{

    public float damage = 10f; //Dolor que hace este hechizo
    float range = 50f; //Rango del hechizo
    float momentum = 50f; //Torque
    public float manaConsumption = 10f; //Mana que consume el hechizo
    public PlayerData player; //Datos personaje
    public Camera FPCamera; //Camara
    public GameObject bengala; //Particulas que salen cuando golpeas algo
    public GameObject particules1; //Particulas del baston
    public GameObject particules2; //""
    public GameObject particules3; //""
    public GameObject particules4; //""
    
    void Start()
    {
        
    }


    void Update()
    {

        if (Input.GetMouseButtonDown(0)) //Click izquierdo disparas hechizo
        {
            Shoot();
        }
        if (Input.GetKeyDown("2")) // Cambias de hechizo
        {
            this.gameObject.GetComponent<HechizoViento>().enabled = true;
            this.gameObject.GetComponent<HechizoFuego>().enabled = false;
            particules1.SetActive(false);
            particules2.SetActive(true);
        }
        if (Input.GetKeyDown("3")) // Cambias de hechizo
        {
            this.gameObject.GetComponent<HechizoFuego>().enabled = false;
            this.gameObject.GetComponent<HechizoAgua>().enabled = true;
            particules2.SetActive(false);
            particules1.SetActive(false);
            particules3.SetActive(true);
        }
        if (Input.GetKeyDown("4")) // Cambias de hechizo
        {
            this.gameObject.GetComponent<HechizoOscuridad>().enabled = true;
            this.gameObject.GetComponent<HechizoFuego>().enabled = false;
            particules1.SetActive(false);
            particules2.SetActive(false);
            particules4.SetActive(true);
            particules3.SetActive(false);
        }

    }

    // Disparar hechizo
    private void Shoot()
    {

        if(player.mana >= manaConsumption) //Si el jugador tiene mana suficiente para lanzar el hechizo funcionara
        {

            player.mana -= manaConsumption; //Restamos mana

            RaycastHit hit; //Raycast para ver donde impacta


            if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
            {
                Debug.DrawLine(FPCamera.transform.position, hit.point, Color.red, 1f);
                print(hit.transform.name);
                

                if (hit.transform.tag == "Disparable") //Si da a algo que pueda sufrir dolor al ser golpeado
                {
                    hit.transform.gameObject.GetComponent<Disparable>().dano(damage);
                    //Vector3 middle = new Vector3(hit.normal.x, )
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
                    hit.transform.gameObject.GetComponent<Pincha>().burns();
                }

                if (hit.transform.tag == "Player") //Si da a otro personaje controlado por otra persona (eso aun no del todo)
                {
                    hit.transform.gameObject.GetComponent<PlayerController>().dano(damage);
                    //Vector3 middle = new Vector3(hit.normal.x, )
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
                }

                GameObject newBengala = Instantiate(bengala);
                newBengala.transform.position = hit.point;
                Destroy(newBengala, 2f);


            }

        }

    }

}
