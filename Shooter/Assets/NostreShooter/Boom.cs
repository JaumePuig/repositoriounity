﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Datos sobre la granada
public class Boom : MonoBehaviour
{

    public float delay = 3f; //Cuento tarda en explotar
    public float radius = 5f; //Radio de explosion
    public float force = 700f; //Fuerza de explosion
    public float damage = 15f; //Dolor causado por la explosion
    public GameEvent pegar; // Evento que causara dolor a quien impacte

    public GameObject explosiveEffect;

    float countdown;
    bool hasExploded = false;

    // Le ponemos un tiempo a la cuenta atras
    void Start()
    {
        countdown = delay;
    }

    // Cuenta atras de la explosion
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f && !hasExploded)
        {
            Explode();
        }
    }

    // Explosion de la granada
    private void Explode()
    {
        Debug.Log("Boom");

        GameObject exp = Instantiate(explosiveEffect, transform.position, transform.rotation);//Explosion como tal

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        // Lo de alrededor se va a la verga con la explosion
        foreach (Collider nearbyObject in colliders)
        {

            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.AddExplosionForce(force, transform.position, radius);
                

                if (rb.gameObject.transform.tag == "Disparable")
                {

                    rb.gameObject.GetComponent<Disparable>().dano(damage);

                }else if (rb.gameObject.transform.tag == "Player")
                {
                    pegar.Raise();
                }

            }

        }

        Destroy(gameObject);
        Destroy(exp, 2f);

    }
}
