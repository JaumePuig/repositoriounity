﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    float damage = 10f; //Dolor que hace el que una granada explote cerca tuya
    float range = 20000f; //Rango de la granada
    float momentum = 200f; //Torque
    public PlayerData player; //Datos del personaje
    public Camera FPCamera; //Camara de nuestro personaje
    public GameObject pocion; //La granada como tal
    public float throwForce = 40f; //Potencia de lanzacion
    public GameEvent pegar; //Evento que provocara dolor a los enemigos

    // Al pulsar la tecla G lanzaras una granada en la direccion que mires
    void Update()
    {
        if (Input.GetKeyDown("g"))
        {
            Shoot();
        }
    }

    // Si la granadas que tenemos actualmente son mayor que 0 podremos lanzar la granada
    private void Shoot()
    {

        if(player.nBombs > 0)
        {

            player.nBombs--; // Restamos la granada

            GameObject newGranada = Instantiate(pocion, transform.position, transform.rotation); //Creamos la granada

            newGranada.GetComponent<Boom>().pegar = pegar; //En el momento de la explosion inflingiremos dolor en los enemigos

            newGranada.GetComponent<Rigidbody>().AddForce(transform.forward * throwForce, ForceMode.VelocityChange); //Lanzacion

        }

    }
}
