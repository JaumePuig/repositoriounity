﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public PlayerData player;
    public int vida;
    public GameObject[] corazones;
    public GameObject mana;
    public bool immortal = false;
    public GameObject ragdoll;

    // Start is called before the first frame update
    void Start()
    {

        player.nBombs = 3;
        player.hp = 100;
        player.mana = 100;
        Invoke("Regen", 1);
        vida = 10;
        
    }

    // Update is called once per frame
    void Update()
    {

        //vida.transform.localScale = new Vector3(player.hp / 100, 1f, 1f);
        mana.transform.localScale = new Vector3(player.mana / 100, 1f, 1f);

    }

    void Regen()
    {

        if(player.mana >= 95)
        {

            player.mana = 100;

        }
        else
        {

            player.mana += 5;

        }

        Invoke("Regen", 1);

    }

    public void dano(float damage)
    {

        if (!immortal)
        {

            player.hp -= damage;

            vida--;
            corazones[vida].SetActive(false);

            if (player.hp <= 0)
            {
                
                /*
                for (int i = 0; i < this.transform.childCount; i++)
                {

                    if(i != 0)
                    {

                        Destroy(this.transform.GetChild(i).gameObject);

                    }

                }
                */

                Destroy(GameObject.Find("LichMeshPlayer"));
                Destroy(GameObject.Find("CanvasPlayer"));
                Destroy(GameObject.Find("DisparosyArmas"));
                Destroy(GameObject.Find("SpinePlayer"));

                GameObject playerRagdoll = Instantiate(ragdoll, this.transform.position, this.transform.rotation);

                //this.gameObject.GetComponent<RigidbodyFirstPersonController>().enabled = false;
                this.enabled = false;

            }

            immortal = true;
            Invoke("Immortality", 1f);

        }

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag.Equals("Muerte"))
        {

            immortal = false;

            dano(100);

        }

    }

    public void Immortality()
    {

        immortal = false;

    }

}
