﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public PlayerData player; //Datos personaje
    public int vida; //Vida personaje
    public GameObject[] corazones; //Array de corazones mostrados por pantalla
    public GameObject mana; //Recurso que consume nuestro personaje al lanzar hechizos
    public bool immortal = false; //Boleano inmortalidad ¯\_(ツ)_/¯
    public GameObject ragdoll; //Cuerpo inerte de nuestro personaje fenecido

    // Datos base de nuestro personaje
    void Start()
    {

        player.nBombs = 3;
        player.hp = 100;
        player.mana = 100;
        Invoke("Regen", 1);
        vida = 10;
        
    }

    // Barra de mana mostrada por pantalla que cambiara cuando gaste o recupere recursos
    void Update()
    {

        //vida.transform.localScale = new Vector3(player.hp / 100, 1f, 1f);
        mana.transform.localScale = new Vector3(player.mana / 100, 1f, 1f);

    }

    // Regeneracion de mana cada segundo
    void Regen()
    {

        if(player.mana >= 95)
        {

            player.mana = 100;

        }
        else
        {

            player.mana += 5;

        }

        Invoke("Regen", 1);

    }

    //Dolor que sufre el personaje al ser golpeado por gente hostil
    public void daño(float damage)
    {

        if (!immortal)
        {

            player.hp -= damage;

            vida--;
            corazones[vida].SetActive(false);

            if (player.hp <= 0)
            {
                
                /*
                for (int i = 0; i < this.transform.childCount; i++)
                {

                    if(i != 0)
                    {

                        Destroy(this.transform.GetChild(i).gameObject);

                    }

                }
                */

                //Destruimos al personaje que estamos moviendo para convertirlo en un cuerpo inerte aka ragdoll
                Destroy(GameObject.Find("LichMeshPlayer"));
                Destroy(GameObject.Find("CanvasPlayer"));
                Destroy(GameObject.Find("DisparosyArmas"));
                Destroy(GameObject.Find("SpinePlayer"));

                GameObject playerRagdoll = Instantiate(ragdoll, this.transform.position, this.transform.rotation);

                //this.gameObject.GetComponent<RigidbodyFirstPersonController>().enabled = false;
                this.enabled = false;

            }

            immortal = true;
            Invoke("Immortality", 1f);

        }

    }

    //Boleano inmortalidad ¯\_(ツ)_/¯
    public void Immortality()
    {

        immortal = false;

    }

}
