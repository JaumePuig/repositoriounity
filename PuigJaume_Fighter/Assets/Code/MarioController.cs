﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MarioController : MonoBehaviour
{
    //Velocidad de movimiento
    public int vel;
    //Fuerza de salto
    public int jumpForce;
    //Prefab de la bola de fuego
    public GameObject fireBall;
    //Texto por si gana Mario Mario
    public Text marioGana;
    //Animator de Mario Mario
    private Animator anim;
    //Booleano por si puede saltar o no
    private bool canJump;
    //Booleano por si esta bloqueando o no
    private bool blocking;
    //Booleano para el primer puñetazo
    private bool punch1;
    //Booleano para el segundo puñetazo (Combo)
    private bool punch2;
    //Booleano para el tercer puñetazo (Combo)
    private bool punch3;
    //Booleano para la primera patada
    private bool kick1;
    //Booleano para la segunda patada (Combo)
    private bool kick2;
    //Booleano para la tercera patada (Combo)
    private bool kick3;
    //Booleano para el puñetazo en bloqueo
    private bool blockPunch;
    //Booleano para la primera patada en bloqueo
    private bool blockKick1;
    //Booleano para la segunda patada en bloqueo (Combo)
    private bool blockKick2;
    //Booleano para la tercera patada en bloqueo (Combo)
    private bool blockKick3;
    //Booleano por si esta disparando Mario Mario
    private bool fire;
    //Booleano de cooldown del disparo
    private bool fireNot;
    //Direccion en la que mira Mario Mario
    private bool direction;
    //Booleano por si Mario Mario ha fenecido
    private bool dead;
    //Booleano por si Mario Mario es golpeado
    private bool golpeado;
    //Booleano por si Mario Mario se alza con la victoria
    private bool victoria;
    //Representacion numerica de la fuerza vital de Mario Mario
    public float vida;
    //Controlador del hermano de Mario Mario, Luigi Mario
    public LuigiController luigi;
    //Delegado en caso de que Mario Mario resulte atacado
    public delegate void OnAtacked(float vida);
    public event OnAtacked OnAtackedEvent;
    //Delegado en caso de que Mario Mario caiga al suelo de forma innecesariamente teatral
    public delegate void OnDead();
    public event OnDead OnDeadEvent;

    // Start is called before the first frame update
    void Start()
    {

        anim = GetComponent<Animator>();
        direction = true;
        dead = false;
        golpeado = false;
        luigi.OnDeadEvent += Victory;
        

        marioGana.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

        //Sincronizamos las variables de Mario Mario con las variables del animador de Mario Mario
        anim.SetFloat("Speed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));
        anim.SetFloat("VerticalSpeed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.y));
        anim.SetBool("Block", blocking);
        anim.SetBool("Punch1", punch1);
        anim.SetBool("Punch2", punch2);
        anim.SetBool("Punch3", punch3);
        anim.SetBool("Kick1", kick1);
        anim.SetBool("Kick2", kick2);
        anim.SetBool("Kick3", kick3);
        anim.SetBool("BlockPunch", blockPunch);
        anim.SetBool("BlockKick1", blockKick1);
        anim.SetBool("BlockKick2", blockKick2);
        anim.SetBool("BlockKick3", blockKick3);
        anim.SetBool("Firing", fire);
        anim.SetBool("Golpeado", golpeado);
        anim.SetBool("Dead", dead);
        anim.SetBool("Victory", victoria);

        //Si es golpeado, Mario Mario no podra atacar
        if (golpeado || dead || victoria)
        {

        }
        else
        {
            //Movimiento
            if (Input.GetKey("d"))
            {

                if (blocking)
                {

                }
                else
                {

                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);

                }
                direction = true;
                transform.localScale = new Vector3(1f, 1f, 1f);

            }
            else if (Input.GetKey("a"))
            {

                if (blocking)
                {

                }
                else
                {

                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);

                }
                direction = false;
                transform.localScale = new Vector3(-1f, 1f, 1f);

            }
            else
            {

                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);

            }

            //Salto
            if (Input.GetKey("w") && canJump)
            {

                this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

                canJump = false;

                blocking = false;

                anim.SetBool("Jumping", true);

            }

            //Bloqueo, inhabilitado en el aire
            if (Input.GetKey("s"))
            {
                if (anim.GetBool("Jumping"))
                {
                    blocking = false;
                }
                else
                {
                    blocking = true;
                }

            }
            else
            {
                blocking = false;
            }


            //Peñetasos
            if (Input.GetKeyDown("m"))
            {

                if (blocking)
                {
                    //Peñetaso en caso de estar bloqueandp
                    if (!blockPunch)
                    {
                        blockKick1 = false;
                        blockKick2 = false;
                        blockKick3 = false;
                        blockPunch = true;
                        StartCoroutine(BlockHit(.3f, 1));
                        Debug.Log("block punch");
                    }

                }
                else
                {
                    //Peñetasos normales, para hacer un combo espamea la misma tecla
                    if (!punch1 && !punch2 && !punch3)
                    {

                        punch1 = true;
                        anim.SetBool("Punch1", punch1);
                        StartCoroutine(Punch(.5f, 1));
                        Debug.Log("puñetaso1");

                    }
                    else if (punch1 && !punch2 && !punch3)
                    {

                        punch1 = false;
                        punch2 = true;
                        StartCoroutine(Punch(.5f, 2));
                        Debug.Log("puñetaso2");
                    }
                    else if (!punch1 && punch2 && !punch3)
                    {

                        punch2 = false;
                        punch3 = true;
                        StartCoroutine(Punch(.5f, 3));
                        Debug.Log("puñetaso3");
                    }
                }



            }

            //Petadas
            if (Input.GetKeyDown("n"))
            {

                if (blocking)
                {
                    //Combo de petadas bloqueando, spamea tecla
                    if (!blockKick1 && !blockKick2 && !blockKick3)
                    {
                        blockPunch = false;
                        blockKick1 = true;
                        StartCoroutine(BlockHit(.5f, 2));
                        Debug.Log("block kick1");
                    }
                    else if (blockKick1 && !blockKick2 && !blockKick3)
                    {
                        blockKick1 = false;
                        blockKick2 = true;
                        StartCoroutine(BlockHit(.5f, 3));
                        Debug.Log("block kick2");
                    }
                    else if (!blockKick1 && blockKick2 && !blockKick3)
                    {
                        blockKick2 = false;
                        blockKick3 = true;
                        StartCoroutine(BlockHit(.5f, 4));
                        Debug.Log("block kick3");
                    }
                    else if (!blockKick1 && !blockKick2 && blockKick3)
                    {
                        blockKick1 = false;
                        blockKick2 = false;
                        blockKick3 = false;
                    }
                }
                else
                {
                    //Combo de petadas normales, spamea tecla
                    if (!kick1 && !kick2 && !kick3)
                    {

                        kick1 = true;
                        anim.SetBool("Punch1", punch1);
                        StartCoroutine(Kick(.5f, 1));
                        Debug.Log("petada1");

                    }
                    else if (kick1 && !kick2 && !kick3)
                    {

                        kick1 = false;
                        kick2 = true;
                        StartCoroutine(Kick(.5f, 2));
                        Debug.Log("petada2");
                    }
                    else if (!kick1 && kick2 && !kick3)
                    {

                        kick2 = false;
                        kick3 = true;
                        StartCoroutine(Kick(.5f, 3));
                        Debug.Log("petada3");
                    }
                    else if (!kick1 && !kick2 && kick3)
                    {
                        kick1 = false;
                        kick2 = false;
                        kick3 = false;
                        Debug.Log("petada fin");
                    }


                }

            }

            //Lanza bolas de fuego que rebotan por el suelo
            if (Input.GetKeyDown("b"))
            {
                if (!punch1 && !punch2 && !punch3 && !blockPunch && !kick1 && !kick2 && !kick3 && !blockKick1 && !blockKick2 && !blockKick3 && !blocking && !fireNot)
                {
                    fire = true;
                    StartCoroutine(EndFire(.5f));
                    GameObject newFire = Instantiate(fireBall);

                    if (!direction)
                    {
                        newFire.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0);
                        newFire.transform.position = new Vector2(this.transform.position.x - 1, this.transform.position.y + 1);
                    }
                    else
                    {
                        newFire.GetComponent<Rigidbody2D>().velocity = new Vector2(3, 0);
                        newFire.transform.position = new Vector2(this.transform.position.x + 1, this.transform.position.y + 1);
                    }

                    fireNot = true;
                    StartCoroutine(DontFire(.5f));
                }


            }
        }

        //Si la fuerza vital de Mario Mario se reduce hasta limites insospechados este activara su autodestruccion
        if (vida==0)
        {
            OnDeadEvent();
            Die();
        }

    }

    //En el caso de ganar, Mario Mario lo celebrará y saldrá un maravilloso texto por pantalla
    private void Victory()
    {

        victoria = true;
        marioGana.gameObject.SetActive(true);

    }

    //Secuencia de autodestruccion, tambien llamada, tirarse al suelo de forma teatral y exagerada
    private void Die()
    {
        OnDeadEvent();
        dead = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si toca el suelo salta
        if (collision.gameObject.tag == "Floor")
        {
            canJump = true;
            anim.SetBool("Jumping", false);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //En caso de no estar bloqueando se activaran los daños de los ataques enemigos
        if (!blocking)
        {
            //Los ataques hacen una cantidad de daño distinta
            if (collision.gameObject.name == "AtaqueL")
            {
                if (blocking)
                {
                    vida -= 2;
                }
                else
                {
                    vida -= 4;
                }

                if (direction)
                {
                    this.transform.position = new Vector2(this.transform.position.x-0.1f, this.transform.position.y);
                }
                else
                {
                    this.transform.position = new Vector2(this.transform.position.x + 0.1f, this.transform.position.y);
                }

                if (vida < 0)
                {
                    vida = 0;
                }

                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }
                golpeado = true;
                StartCoroutine(Pegame(.2f));
            }
            else if (collision.gameObject.name == "PatadaL")
            {

                if (blocking)
                {
                    vida -= 3;
                }
                else
                {
                    vida -= 6;
                }

                if (direction)
                {
                    this.transform.position = new Vector2(this.transform.position.x - 0.1f, this.transform.position.y);
                }
                else
                {
                    this.transform.position = new Vector2(this.transform.position.x + 0.1f, this.transform.position.y);
                }

                if (vida < 0)
                {
                    vida = 0;
                }

                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }
                golpeado = true;
                StartCoroutine(Pegame(.2f));
            }
            else if (collision.gameObject.name == "LuigiBall")
            {

                if (blocking)
                {
                    vida -= 1;
                }
                else
                {
                    vida -= 2;
                }

                if (vida < 0)
                {
                    vida = 0;
                }

                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }
                golpeado = true;
                StartCoroutine(Pegame(.2f));
            }
            else if (collision.gameObject.name == "Lava")
            {
                //La lava es mala, no la toques
                vida = 0;
                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }

            }


        }
        else
        {
            //Que tal vas?
        }
        

    }

    //ECOrutina del combo de puñetazos y patadas bloqueando
    IEnumerator BlockHit(float f, int n)
    {

        yield return new WaitForSeconds(f);
        if (n == 1)
        {
            blockPunch = false;
        }
        else if (n == 2)
        {
            blockKick1 = false;
        }
        else if (n == 3)
        {
            blockKick2 = false;
        }
        else if (n == 4)
        {
            blockKick3 = false;
        }

    }
    //ECOrutina del combo de puñetazos
    IEnumerator Punch(float f, int n)
    {

        yield return new WaitForSeconds(f);
        if (n == 1)
        {
            punch1 = false;
        }
        else if (n == 2)
        {
            punch2 = false;
        }
        else if (n == 3)
        {
            punch3 = false;
        }

    }
    //ECOrutina del combo de patadas
    IEnumerator Kick(float f, int n)
    {

        yield return new WaitForSeconds(f);
        if (n == 1)
        {
            kick1 = false;
        }
        else if (n == 2)
        {
            kick2 = false;
        }
        else if (n == 3)
        {
            kick3 = false;
        }

    }
    //ECOrutina del cooldown del proyectil
    IEnumerator EndFire(float f)
    {

        yield return new WaitForSeconds(f);
        fire = false;

    }
    //ECOrutina del cooldown del proyectil
    IEnumerator DontFire(float f)
    {

        yield return new WaitForSeconds(f);
        fireNot = false;

    }
    //ECOrutina del cooldown de ser pegado
    IEnumerator Pegame(float f)
    {

        yield return new WaitForSeconds(f);

        golpeado = false;

    }
    
}
