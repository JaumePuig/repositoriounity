﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puenteController : MonoBehaviour
{

    public bool destroyed;
    public hachaController hacha;
    // Start is called before the first frame update
    void Start()
    {
        //En caso de que la vida del hacha llegue a cero este puente se destruira y empezara a caer
        destroyed = false;
        hacha.OnAtackedEvent += getDestroyed;
    }

    // Update is called once per frame
    void Update()
    {

        //Tras ser destruido el puente, se regenerara al cabo de 10 segundos
        if (destroyed)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -2);
            StartCoroutine(Regenerate(10f));
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }
    }

    IEnumerator Regenerate(float f)
    {

        yield return new WaitForSeconds(f);
        destroyed = false;
        this.gameObject.transform.position = new Vector2(1.031f, -1.771f);
        
    }

    void getDestroyed()
    {

        destroyed = true;

    }

}
