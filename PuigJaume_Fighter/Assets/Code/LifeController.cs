﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeController : MonoBehaviour
{

    public MarioController mario;
    public LuigiController luigi;
    public GameObject vidaMario;
    public GameObject vidaLuigi;

    // Start is called before the first frame update
    void Start()
    {
        
        mario.OnAtackedEvent += MarioDamage;
        luigi.OnAtackedEvent += LuigiDamage;

    }

    // Update is called once per frame
    void Update()
    {
        


    }

    //Si los jugadores sufren daño se reduciran las barras de vida
    void MarioDamage(float hp)
    {
        vidaMario.transform.localScale = new Vector2(hp / 100, vidaMario.transform.localScale.y);
    }

    void LuigiDamage(float hp)
    {
        vidaLuigi.transform.localScale = new Vector2(hp / 100, vidaLuigi.transform.localScale.y);
    }

}
