﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireController : MonoBehaviour
{
    public int vel;

    // Start is called before the first frame update
    void Start()
    {
        //Al cabo de 5 segundos de crearse el objeto este sera destruido.
        StartCoroutine(Die(5f));
    }

    // Update is called once per frame
    void Update()
    {

        //this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        //si choca con algo que no sea ni el suelo, ni el jugador, ni una de sus bolas de fuegor se destruye el objeto.

        if (collision.gameObject.tag == "Floor" || collision.gameObject.name == "fireBallMario" || collision.gameObject.tag == "Player")
        {

        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    
    IEnumerator Die(float f)
    {
        
        yield return new WaitForSeconds(f);
        Destroy(this.gameObject);

    }

}
