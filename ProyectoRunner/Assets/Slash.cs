﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : MonoBehaviour
{

    public int horizontalVelocity;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //movimiento del slash.
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //el objeto se destruye si colisiona contra un objeto Trap o Enemy.
        if (collision.gameObject.tag == "Trap" || collision.gameObject.tag == "Enemy")
        {
            GameObject.Destroy(this.gameObject);
        }

    }

}
