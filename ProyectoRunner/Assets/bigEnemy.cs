﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bigEnemy : MonoBehaviour
{

    int life = 1;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        


    }
    // Función que detecta colisiones, usada en este caso para detectar si se ha colisionado con un objeto slash, y en tal caso se resta 1 a la vida del objeto, al llegar a 0 éste objeto es eliminado.
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Slash")
        {

            if (life>0)
            {
                life--;
            }
            else
            {
                Controller.puntos += 20;
                GameObject.Destroy(this.gameObject);
            }
            

        }

    }
}
