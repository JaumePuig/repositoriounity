﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatMover : MonoBehaviour
{

    public int horizontalVelocity;
    public bool direccion;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // if para especificar la direccion del movimiento del objeto.
        if (direccion)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (!direccion)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);
        }


    }

    // Función que detecta colisiones, usada en este caso para detectar si se ha colisionado con un objeto slash, y en tal caso destruir éste objeto.
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Slash")
        {
            Controller.puntos += 15;
            GameObject.Destroy(this.gameObject);

        }

    }

    // Función que detecta objetos trigger, usada en este caso para detectar los límites del movimiento del objeto.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.gameObject.tag == "Limit")
        {
            direccion = !direccion;
        }

    }

}
