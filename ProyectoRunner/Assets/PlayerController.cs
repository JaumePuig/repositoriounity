﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //Controles
    //SPACE para saltar
    //A para disparar
    //F para suicidarse


    public int jumpForce;
    public int horizontalVelocity;
    public bool canJump;
    public bool canAttack;
    public GameObject slash;
    public int cunt;
    // Start is called before the first frame update


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") && canJump)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));

            canJump = false;
        }

        if (Input.GetKeyDown("a") && canAttack)
        {
            GameObject newBlock = Instantiate(slash);
            newBlock.transform.position = new Vector2(this.gameObject.transform.position.x+1, this.gameObject.transform.position.y);
            canAttack = false;
        }

        if (Input.GetKeyDown("f"))
        {
            Death();
        }
        // Éste es el contador utilizado para el cooldown del ataque slash, que se puede hacer cada 60 frames.
        if (cunt >= 60)
        {
            cunt = 0;
            canAttack = true;
        }

        this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);

        cunt++;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //En caso de colisionar con un objeto Bound el personaje podra usar su salto.
        if(collision.gameObject.tag == "Bound")
        {
            canJump = true;

        }
        //En caso de colisionar con un objeto Enemy o Trap el personaje activará la función Death().
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Trap")
        {
            //GameObject.Destroy(this.gameObject);

            Death();

        }

    }
    //La función Death sirve para cambiar a la escena de muerte.
    private void Death()
    {
        SceneManager.LoadScene("Death");
    }
}
