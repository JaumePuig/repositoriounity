﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Muertisimo : MonoBehaviour
{

    public Text score;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Escena de muerte del personaje, donde se muestran los puntos finales y se puede reiniciar el juego pulsando ENTER.
        score.text = "Score:" + Controller.puntos;

        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene("Juego");
        }

    }
}
