﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropSlot : MonoBehaviour, IDropHandler
{
    public GameObject item;
    public GameObject[] pokimons;

    public void OnDrop(PointerEventData eventData)
    {
        if (!item)
        {
            item = drag2.ItemDrag;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
        }
        else
        {

            if(item.tag == drag2.ItemDrag.tag)
            {

                if(item.tag == "Charmander")
                {

                    Destroy(item);
                    Destroy(drag2.ItemDrag);
                    GameObject newPokimon = Instantiate(pokimons[0]);
                    newPokimon.transform.SetParent(this.transform, false);
                    newPokimon.transform.position = transform.position;

                }
                else if (item.tag == "Charmeleon")
                {

                    Destroy(item);
                    Destroy(drag2.ItemDrag);
                    GameObject newPokimon = Instantiate(pokimons[1]);
                    newPokimon.transform.SetParent(this.transform, false);
                    newPokimon.transform.position = transform.position;

                }
                else if (item.tag == "Bulbasaur")
                {

                    Destroy(item);
                    Destroy(drag2.ItemDrag);
                    GameObject newPokimon = Instantiate(pokimons[2]);
                    newPokimon.transform.SetParent(this.transform, false);
                    newPokimon.transform.position = transform.position;

                }
                else if (item.tag == "Ivysaur")
                {

                    Destroy(item);
                    Destroy(drag2.ItemDrag);
                    GameObject newPokimon = Instantiate(pokimons[3]);
                    newPokimon.transform.SetParent(this.transform, false);
                    newPokimon.transform.position = transform.position;

                }
                else if (item.tag == "Squirtle")
                {

                    Destroy(item);
                    Destroy(drag2.ItemDrag);
                    GameObject newPokimon = Instantiate(pokimons[4]);
                    newPokimon.transform.SetParent(this.transform, false);
                    newPokimon.transform.position = transform.position;

                }
                else if (item.tag == "Wartortle")
                {

                    Destroy(item);
                    Destroy(drag2.ItemDrag);
                    GameObject newPokimon = Instantiate(pokimons[5]);
                    newPokimon.transform.SetParent(this.transform, false);
                    newPokimon.transform.position = transform.position;

                }

            }


        }

    }
    

    private void Update()
    {
        if (item != null && item.transform.parent != transform)
        {
            Debug.Log("Remover");
            item = null;
        }
    }
}
