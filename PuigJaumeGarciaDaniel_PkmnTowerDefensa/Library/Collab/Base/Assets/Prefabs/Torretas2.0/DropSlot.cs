﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropSlot : MonoBehaviour, IDropHandler
{
    public GameObject item;
    public GameObject[] pokimons;

    public void OnDrop(PointerEventData eventData)
    {
        if (!item)
        {
            item = drag2.ItemDrag;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
        }
        else
        {

            if(item.tag == drag2.ItemDrag.tag)
            {

                if(item.tag == "Charmander")
                {

                    Destroy(item);
                    Destroy(drag2.ItemDrag);
                    GameObject newPokimon = Instantiate(pokimons[1]);
                    newPokimon.transform.SetParent(this.transform, false);
                    newPokimon.transform.position = transform.position;

                }

            }


        }

    }
    

    private void Update()
    {
        if (item != null && item.transform.parent != transform)
        {
            Debug.Log("Remover");
            item = null;
        }
    }
}
