﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class snoruntController : MonoBehaviour
{
    //Booleano que dice si el pokemon esta de espaldas o de cara
    private bool deCara;
    //Animator del pokemon
    private Animator anim;
    //Fuerza vital de la criatura, A.K.A. vida
    public int vida;
    //Booleano por si es el primer enemigo spawneado
    public bool first;
    //Booleano que bloquea el cambio del eje de las y
    public bool blockedY;
    //Booleano que bloquea el cambio del eje de las x
    public bool blockedX;
    //Contador de las almas inocentes que cosechas, A.K.A. recurso
    public Text almas;
    //Contador de la vida del centro pokemon, no se me ocurre otra forma de ponerlo
    public Text vidaCentro;

    // Start is called before the first frame update
    void Start()
    {
        //Le damos el valor necesario a las variables
        deCara = true;
        anim = GetComponent<Animator>();
        blockedX = false;
        blockedY = false;
        anim.SetBool("Arriba", true);
        this.transform.localScale = new Vector3(1f, 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {

        //Hola marc, feliz navidad y feliz ano nuevo

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "cambioX")
        {

            if (blockedX)
            {

            }
            else
            {

                //Si colisiona con un objeto con el tag de cambioX y el eje de las x no esta bloqueado,
                //el pokemon se girara y se bloqueara temporalmente el eje para que no se pueda volver loco girandose a veces

                this.transform.localScale = new Vector3(-(this.transform.localScale.x), 1f, 1f);
                blockedX = true;
                StartCoroutine(BloquearX(.2f));

            }
            
        }

        if (collision.gameObject.tag == "cambioY")
        {

            if (blockedY)
            {

            }
            else
            {

                //Si colisiona con un objeto con el tag de cambioY y el eje de las y no esta bloqueado,
                //el pokemon se dara la vuelta y te dara la espalda de forma fria o te mirara y juzgara

                deCara = !deCara;
                anim.SetBool("Arriba", deCara);
                blockedY = true;
                StartCoroutine(BloquearY(.2f));

            }
            

        }

        if (collision.gameObject.tag == "Ataque")
        {

            //Al colisionar con un objeto de proyectil el pokemon perdera vida en funcion del tipo de proyectil
            //No hay ninguna razon en concreto para las cantidades de daño que hacen cada proyectil

            if(collision.gameObject.name == "chidori(Clone)")
            {
                vida = vida - 10;
            }

            if (collision.gameObject.name == "fireball(Clone)")
            {
                vida = vida - 15;
            }

            if (collision.gameObject.name == "waterball(Clone)")
            {
                vida = vida - 12;
            }

            if (collision.gameObject.name == "energyball(Clone)")
            {
                vida = vida - 13;
            }

            //Si la vida se reduce a 0 o menos, el pokemon morira con un pequeño lapso de tiempo entre medias
            if (vida <= 0)
            {
                StartCoroutine(Muerte(.3f));
            }
        }

        if (collision.gameObject.name == "Muerte")
        {
            //Si colisiona con un objeto muerte, la vida se reducira a 0 y se llamara a la ecorutina del sufrimiento
            vida = 0;
            StartCoroutine(Dolor(.2f));
        }

    }
    //Bloquea temporalmente el eje de las x
    IEnumerator BloquearX(float f)
    {
        
        yield return new WaitForSeconds(f);
        blockedX = false;

    }
    //Bloquea temporalmente el eje de las y
    IEnumerator BloquearY(float f)
    {

        yield return new WaitForSeconds(f);
        blockedY = false;

    }

    IEnumerator Dolor(float f)
    {
        //Se suicida el pokemon, pero antes le quita vida al centro pokemon
        yield return new WaitForSeconds(f);
        vidaCentro.text = int.Parse(vidaCentro.text.ToString()) - 10 + "";
        Destroy(this.gameObject);

    }

    IEnumerator Muerte(float f)
    {
        //El pokemon se suicida y cosechas su alma
        yield return new WaitForSeconds(f);
        almas.text = int.Parse(almas.text.ToString())+1+"";
        Destroy(this.gameObject);

    }

}
