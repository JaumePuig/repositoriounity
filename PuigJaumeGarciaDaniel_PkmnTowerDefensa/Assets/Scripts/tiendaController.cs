﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class tiendaController : MonoBehaviour
{

    public GameObject[] pokimons;//Lista de pokemons
    public Text almas;//Texto en el cual podras ver la cantidad de recursos que tienes actualmente
    public Text costacion;//Texto en el cual podras ver el coste para comprar pokemones
    public int coste;//El coste que ira aumentando con cada compra

    public GameObject Char;//Pasamos un prefab de Charmander
    public GameObject Bulb;//Pasamos un prefab de Bulbasaur
    public GameObject Squirt;//Pasamos un prefab de Squirtle
    public GameObject Pika;//Pasamos un prefab de Pikachu

    //Decimos que el coste base al principio sea de 1
    void Start()
    {
        coste = 1;
    }

    // Update is called once per frame
    void Update()
    {
        

    }

    //Al pulsar el boton de Charmander realizaremos su compra
    public void comprarCharmander()
    {
        //Si los recursos son mayor o igual al coste actual podremos comprar
        if (int.Parse(almas.text.ToString()) >= coste)
        {

            GameObject newPokimon = Instantiate(pokimons[0]);//Generamos un nuevo prefab del pokemon elegido
            newPokimon.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);//Hacemos que el prefab este en canvas o sino no se veria ya que es una imagen
            newPokimon.transform.position = new Vector3(-8.7f, -7.9f, 0f);//Lo colocamos en la zona de la tienda en la que apareceran nuestras adquisiciones
            almas.text = (int.Parse(almas.text.ToString()) - coste) + "";//Restamos el coste de nuestros recursos
            coste++;//Incrementamos el coste
            costacion.text = coste + "";//Actualizamos el texto del coste

        }


    }

    //Al pulsar el boton de Bulbasaur realizaremos su compra
    public void comprarBulbasaur()
    {
        //Si los recursos son mayor o igual al coste actual podremos comprar
        if (int.Parse(almas.text.ToString()) >= coste)
        {

            GameObject newPokimon = Instantiate(pokimons[1]);//Generamos un nuevo prefab del pokemon elegido
            newPokimon.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);//Hacemos que el prefab este en canvas o sino no se veria ya que es una imagen
            newPokimon.transform.position = new Vector3(-8.7f, -7.9f, 0f);//Lo colocamos en la zona de la tienda en la que apareceran nuestras adquisiciones
            almas.text = (int.Parse(almas.text.ToString()) - coste) + "";//Restamos el coste de nuestros recursos
            coste++;//Incrementamos el coste
            costacion.text = coste + "";//Actualizamos el texto del coste

        }

    }

    //Al pulsar el boton de Squirtle realizaremos su compra
    public void comprarSquirtle()
    {
        //Si los recursos son mayor o igual al coste actual podremos comprar
        if (int.Parse(almas.text.ToString()) >= coste)
        {

            GameObject newPokimon = Instantiate(pokimons[2]);//Generamos un nuevo prefab del pokemon elegido
            newPokimon.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);//Hacemos que el prefab este en canvas o sino no se veria ya que es una imagen
            newPokimon.transform.position = new Vector3(-8.7f, -7.9f, 0f);//Lo colocamos en la zona de la tienda en la que apareceran nuestras adquisiciones
            almas.text = (int.Parse(almas.text.ToString()) - coste) + "";//Restamos el coste de nuestros recursos
            coste++;//Incrementamos el coste
            costacion.text = coste + "";//Actualizamos el texto del coste

        }

    }

    //Al pulsar el boton de Pikachu realizaremos su compra
    public void comprarPikachu()
    {
        //Si los recursos son mayor o igual al coste actual podremos comprar
        if (int.Parse(almas.text.ToString()) >= coste)
        {

            GameObject newPokimon = Instantiate(pokimons[3]);//Generamos un nuevo prefab del pokemon elegido
            newPokimon.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);//Hacemos que el prefab este en canvas o sino no se veria ya que es una imagen
            newPokimon.transform.position = new Vector3(-8.7f, -7.9f, 0f);//Lo colocamos en la zona de la tienda en la que apareceran nuestras adquisiciones
            almas.text = (int.Parse(almas.text.ToString()) - coste) + "";//Restamos el coste de nuestros recursos
            coste++;//Incrementamos el coste
            costacion.text = coste + "";//Actualizamos el texto del coste

        }

    }

}
