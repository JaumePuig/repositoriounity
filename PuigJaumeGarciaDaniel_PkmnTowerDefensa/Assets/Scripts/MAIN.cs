﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MAIN : MonoBehaviour
{
    //Al pulsar el boton start te llevara a la escena de seleccion de mapas
    public void start()
    {
        SceneManager.LoadScene("menu");
    }
}
