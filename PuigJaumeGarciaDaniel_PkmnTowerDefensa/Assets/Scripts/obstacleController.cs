﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleController : MonoBehaviour
{

    bool cambia;

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        cambia = false;
        StartCoroutine(Cambiar(5f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Cambiar(float f)
    {

        yield return new WaitForSeconds(f);
        cambia = !cambia;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = cambia;
        AstarPath.active.Scan();
        StartCoroutine(Cambiar(f));

    }

}
