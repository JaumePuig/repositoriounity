﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropSlot : MonoBehaviour, IDropHandler
{
    public GameObject item;//elementos que estamos arrastrando actualmente
    public GameObject[] pokimons;//lisata de las evoluciones de nuestros pokemones

    //Cuando acabamos el drag y soltamos lo colocamos en un slot vacio o en uno en el cual haya un elemento igual
    public void OnDrop(PointerEventData eventData)
    {
        if (!item)//Si el slot esta vacio, el elemento se quedara en el slot y no volvera a su posicion inicial
        {
            item = drag2.ItemDrag;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
        }
        else//Si hay algun elemento miraremos si los dos elementos son iguales
        {

            if(item.tag == drag2.ItemDrag.tag) //Si dos Pokemones son iguales eliminaremos ambos elementos y crearemos uno nuevo que sera su evolucion
            
            {
                //Si los dos elementos son Charmander evolucionara
                if(item.tag == "Charmander")
                {
                    Destroy(item);//Destruimos el elemento que habia en el slot
                    Destroy(drag2.ItemDrag);//Destruimos el elemento que estamos arrastrando
                    item = Instantiate(pokimons[0]);//Creamos un nuevo elemento
                    item.transform.SetParent(this.transform, false);//Colocamos este elemento en la posicion del elemento que ya estaba en un slot
                    item.transform.position = transform.position;

                }
                else if (item.tag == "Charmeleon")//Si los dos elementos son Charmeleons evolucionara
                {
                    Destroy(item);//Destruimos el elemento que habia en el slot
                    Destroy(drag2.ItemDrag);//Destruimos el elemento que estamos arrastrando
                    item = Instantiate(pokimons[1]);//Creamos un nuevo elemento
                    item.transform.SetParent(this.transform, false);//Colocamos este elemento en la posicion del elemento que ya estaba en un slot
                    item.transform.position = transform.position;

                }
                else if (item.tag == "Charizard")//Si los dos elementos son Charizards megaevolucionara
                {
                    Destroy(item);//Destruimos el elemento que habia en el slot
                    Destroy(drag2.ItemDrag);//Destruimos el elemento que estamos arrastrando
                    item = Instantiate(pokimons[6]);//Creamos un nuevo elemento
                    item.transform.SetParent(this.transform, false);//Colocamos este elemento en la posicion del elemento que ya estaba en un slot
                    item.transform.position = transform.position;

                }
                else if (item.tag == "Bulbasaur")//Si los dos elementos son Bulbasurs evolucionara
                {
                    Destroy(item);//Destruimos el elemento que habia en el slot
                    Destroy(drag2.ItemDrag);//Destruimos el elemento que estamos arrastrando
                    item = Instantiate(pokimons[2]);//Creamos un nuevo elemento
                    item.transform.SetParent(this.transform, false);//Colocamos este elemento en la posicion del elemento que ya estaba en un slot
                    item.transform.position = transform.position;

                }
                else if (item.tag == "Ivysaur")//Si los dos elementos son Ivysaurs evolucionara
                {
                    Destroy(item);//Destruimos el elemento que habia en el slot
                    Destroy(drag2.ItemDrag);//Destruimos el elemento que estamos arrastrando
                    item = Instantiate(pokimons[3]);//Creamos un nuevo elemento
                    item.transform.SetParent(this.transform, false);//Colocamos este elemento en la posicion del elemento que ya estaba en un slot
                    item.transform.position = transform.position;

                }
                else if (item.tag == "Squirtle")//Si los dos elementos son Squirtles evolucionara
                {
                    Destroy(item);//Destruimos el elemento que habia en el slot
                    Destroy(drag2.ItemDrag);//Destruimos el elemento que estamos arrastrando
                    item = Instantiate(pokimons[4]);//Creamos un nuevo elemento
                    item.transform.SetParent(this.transform, false);//Colocamos este elemento en la posicion del elemento que ya estaba en un slot
                    item.transform.position = transform.position;

                }
                else if (item.tag == "Wartortle")//Si los dos elementos son Wartortles evolucionara
                {
                    Destroy(item);//Destruimos el elemento que habia en el slot
                    Destroy(drag2.ItemDrag);//Destruimos el elemento que estamos arrastrando
                    item = Instantiate(pokimons[5]);//Creamos un nuevo elemento
                    item.transform.SetParent(this.transform, false);//Colocamos este elemento en la posicion del elemento que ya estaba en un slot
                    item.transform.position = transform.position;

                }

            }


        }

    }
    
    //Al quitar un elemento de una posicion le diremos al slot que vuelve a estar vacio y que es posible colocar un nuevo elemento en el
    private void Update()
    {
        if (item != null && item.transform.parent != transform)
        {
            Debug.Log("Remover");
            item = null;//Pasamos el item a null
        }
    }
}
